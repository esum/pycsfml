#from Export cimport *


cdef extern from "<SFML/Window/JoystickIdentification.h>":

	ctypedef struct sfJoystickIdentification:
		const char*  name
		unsigned int vendorId
		unsigned int productId
