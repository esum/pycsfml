#from Export cimport *
from .Types cimport *
from System.Vector3 cimport *


cdef extern from "<SFML/Window/Sensor.h>":

	ctypedef enum sfSensorType:
		sfSensorAccelerometer,
		sfSensorGyroscope,
		sfSensorMagnetometer,
		sfSensorGravity,
		sfSensorUserAcceleration,
		sfSensorOrientation,
		sfSensorCount

	bint sfSensor_isAvailable(sfSensorType sensor)
	void sfSensor_setEnabled(sfSensorType sensor, bint enabled)
	sfVector3f sfSensor_getValue(sfSensorType sensor)
