cdef extern from "<SFML/Audio/Types.h>":

	ctypedef struct sfMusic sfMusic;
	ctypedef struct sfSound sfSound;
	ctypedef struct sfSoundBuffer sfSoundBuffer;
	ctypedef struct sfSoundBufferRecorder sfSoundBufferRecorder;
	ctypedef struct sfSoundRecorder sfSoundRecorder;
	ctypedef struct sfSoundStream sfSoundStream;
