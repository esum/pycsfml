#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Types cimport *
from System.InputStream cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Image.h>":

	sfImage* sfImage_create(unsigned int width, unsigned int height)
	sfImage* sfImage_createFromColor(unsigned int width, unsigned int height, sfColor color)
	sfImage* sfImage_createFromPixels(unsigned int width, unsigned int height, const unsigned char* pixels)
	sfImage* sfImage_createFromFile(const char* filename)
	sfImage* sfImage_createFromMemory(const void* data, size_t size)
	sfImage* sfImage_createFromStream(sfInputStream* stream)
	sfImage* sfImage_copy(const sfImage* image)
	void sfImage_destroy(sfImage* image)
	bint sfImage_saveToFile(const sfImage* image, const char* filename)
	sfVector2u sfImage_getSize(const sfImage* image)
	void sfImage_createMaskFromColor(sfImage* image, sfColor color, unsigned char alpha)
	void sfImage_copyImage(sfImage* image, const sfImage* source, unsigned int destX, unsigned int destY, sfIntRect sourceRect, bint applyAlpha)
	void sfImage_setPixel(sfImage* image, unsigned int x, unsigned int y, sfColor color)
	sfColor sfImage_getPixel(const sfImage* image, unsigned int x, unsigned int y)
	const unsigned char* sfImage_getPixelsPtr(const sfImage* image)
	void sfImage_flipHorizontally(sfImage* image)
	void sfImage_flipVertically(sfImage* image)
