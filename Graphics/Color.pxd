#from Export cimport *


cdef extern from "<SFML/Graphics/Color.h>":

	ctypedef struct sfColor:
		unsigned char r
		unsigned char g
		unsigned char b
		unsigned char a

	sfColor sfBlack
	sfColor sfWhite
	sfColor sfRed
	sfColor sfGreen
	sfColor sfBlue
	sfColor sfYellow
	sfColor sfMagenta
	sfColor sfCyan
	sfColor sfTransparent
	sfColor sfColor_fromRGB(unsigned char red, unsigned char green, unsigned char blue)
	sfColor sfColor_fromRGBA(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
	sfColor sfColor_fromInteger(unsigned int color)
	unsigned int sfColor_toInteger(sfColor color)
	sfColor sfColor_add(sfColor color1, sfColor color2)
	sfColor sfColor_subtract(sfColor color1, sfColor color2)
	sfColor sfColor_modulate(sfColor color1, sfColor color2)
