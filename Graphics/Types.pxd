cdef extern from "<SFML/Graphics/Types.h>":

	ctypedef struct sfCircleShape:
		pass
	ctypedef struct sfConvexShape:
		pass
	ctypedef struct sfFont:
		pass
	ctypedef struct sfImage:
		pass
	ctypedef struct sfShader:
		pass
	ctypedef struct sfRectangleShape:
		pass
	ctypedef struct sfRenderTexture:
		pass
	ctypedef struct sfRenderWindow:
		pass
	ctypedef struct sfShape:
		pass
	ctypedef struct sfSprite:
		pass
	ctypedef struct sfText:
		pass
	ctypedef struct sfTexture:
		pass
	ctypedef struct sfTransformable:
		pass
	ctypedef struct sfVertexArray:
		pass
	ctypedef struct sfView:
		pass
