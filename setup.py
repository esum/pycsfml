from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import os, sys

if sys.platform == 'darwin':
    from distutils import sysconfig
    vars = sysconfig.get_config_vars()
    vars['LDSHARED'] = vars['LDSHARED'].replace('-bundle', '-dynamiclib')

setup(
    package_data = {
        'sf': ['*.pxd'],
    },
    ext_modules = cythonize(
        Extension('sf', ['sf.pyx'],
        libraries=['csfml-system', 'csfml-window', 'csfml-graphics']),
        include_path=[os.getcwd()])
)
