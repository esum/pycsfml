from System cimport *
from Window cimport *
from Graphics cimport *
from cpython.mem cimport PyMem_Malloc as malloc, PyMem_Free as free


cdef class Vector2i:

	cdef sfVector2i *_Vector2i

	def __cinit__(self, int x, int y):
		self._Vector2i = <sfVector2i*> malloc(sizeof(sfVector2i))
		self._Vector2i[0].x = x
		self._Vector2i[0].y = y

	def __dealloc__(self):
		free(self._Vector2i)

	property x:
		def __get__(self):
			return self._Vector2i[0].x

		def __set__(self, int x):
			self._Vector2i[0].x = x

	property y:
		def __get__(self):
			return self._Vector2i[0].y

		def __set__(self, int y):
			self._Vector2i[0].y = y

	def __add__(self, other):
		return Vector2i(self.x + other.x, self.y + other.y)

	def __sub__(self, other):
		return Vector2i(self.x - other.x, self.y - other.y)

	def __neg__(self):
		return Vector2i(-self.x, -self.y)

	def __getitem__(self, unsigned int index):
		if index >= 2:
			raise IndexError
		elif index:
			return self.y
		else:
			return self.x

	def __copy__(self):
		return Vector2i(self.x, self.y)

	def __repr__(self):
		return "Vector2i({0}, {1})".format(self.x, self.y)


cdef class Vector2u:

	cdef sfVector2u *_Vector2u

	def __cinit__(self, unsigned int x, unsigned int y):
		self._Vector2u = <sfVector2u*> malloc(sizeof(sfVector2i))
		self._Vector2u[0].x = x
		self._Vector2u[0].y = y

	def __dealloc__(self):
		free(self._Vector2u)

	property x:
		def __get__(self):
			return self._Vector2u[0].x

		def __set__(self, unsigned int x):
			self._Vector2u[0].x = x

	property y:
		def __get__(self):
			return self._Vector2u[0].y

		def __set__(self, unsigned int y):
			self._Vector2u[0].y = y

	def __add__(self, other):
		return Vector2u(self.x + other.x, self.y + other.y)

	def __sub__(self, other):
		return Vector2u(self.x - other.x, self.y - other.y)

	def __neg__(self):
		return Vector2u(-self.x, -self.y)

	def __getitem__(self, unsigned int index):
		if index >= 2:
			raise IndexError
		elif index:
			return self.y
		else:
			return self.x

	def __copy__(self):
		return Vector2u(self.x, self.y)

	def __repr__(self):
		return "Vector2u({0}, {1})".format(self.x, self.y)


cdef class Vector2f:

	cdef sfVector2f *_Vector2f

	def __cinit__(self, float x, float y):
		self._Vector2f = <sfVector2f*> malloc(sizeof(sfVector2f))
		self._Vector2f[0].x = x
		self._Vector2f[0].y = y

	def __dealloc__(self):
		free(self._Vector2f)

	property x:
		def __get__(self):
			return self._Vector2f[0].x

		def __set__(self, float x):
			self._Vector2f[0].x = x

	property y:
		def __get__(self):
			return self._Vector2f[0].y

		def __set__(self, float y):
			self._Vector2f[0].y = y

	def __add__(self, other):
		return Vector2f(self.x + other.x, self.y + other.y)

	def __sub__(self, other):
		return Vector2f(self.x - other.x, self.y - other.y)

	def __neg__(self):
		return Vector2f(-self.x, -self.y)

	def __getitem__(self, unsigned int index):
		if index >= 2:
			raise IndexError
		elif index:
			return self.y
		else:
			return self.x

	def __copy__(self):
		return Vector2f(self.x, self.y)

	def __repr__(self):
		return "Vector2f({0}, {1})".format(self.x, self.y)


cdef class Vector3f:

	cdef sfVector3f *_Vector3f

	def __cinit__(self, float x, float y, float z):
		self._Vector3f = <sfVector3f*> malloc(sizeof(sfVector3f))
		self._Vector3f[0].x = x
		self._Vector3f[0].y = y
		self._Vector3f[0].z = z

	def __dealloc__(self):
		free(self._Vector3f)

	property x:
		def __get__(self):
			return self._Vector3f[0].x

		def __set__(self, float x):
			self._Vector3f[0].x = x

	property y:
		def __get__(self):
			return self._Vector3f[0].y

		def __set__(self, float y):
			self._Vector3f[0].y = y

	property z:
		def __get__(self):
			return self._Vector3f[0].z

		def __set__(self, float z):
			self._Vector3f[0].z = z

	def __getitem__(self, unsigned int index):
		if index >= 3:
			raise IndexError
		elif index == 2:
			return self.z
		elif index:
			return self.y
		else:
			return self.x

	def __repr__(self):
		return "Vector3f({0}, {1}, {2})".format(self.x, self.y, self.z)


cdef class Time:

	cdef sfTime *_Time

	def __cinit__(self, long long microseconds=0):
		self._Time = <sfTime*> malloc(sizeof(sfTime))
		self._Time[0].microseconds = microseconds

	def __dealloc__(self):
		free(self._Time)

	property seconds:
		def __get__(self):
			return sfTime_asSeconds(self._Time[0])

	property milliseconds:
		def __get__(self):
			return sfTime_asMilliseconds(self._Time[0])

	property microseconds:
		def __get__(self):
			return sfTime_asMicroseconds(self._Time[0])

	def __repr__(self):
		return "Time({0}μs)".format(self.microseconds)


cdef class Clock:

	cdef sfClock *_Clock

	def __cinit__(self, bint init=True):
		if init:
			self._Clock = sfClock_create()

	def __dealloc__(self):
		if self._Clock is not NULL:
			sfClock_destroy(self._Clock)

	def __copy__(self):
		cdef Clock clock = Clock(init=False)
		clock._Clock = sfClock_copy(self._Clock)
		return clock

	property elapsed_time:
		def __get__(self):
			cdef Time time = Time()
			time._Time[0] = sfClock_getElapsedTime(self._Clock)
			return time

	cpdef Time restart(self):
		cdef Time time = Time()
		time._Time[0] = sfClock_restart(self._Clock)
		return time

	def __repr__(self):
		return "Clock() ({0})".format(repr(self.elapsed_time))


cdef class JoystickIdentification:

	cdef sfJoystickIdentification *_JoystickIdentification

	cdef init(self, char* name):
		self._JoystickIdentification[0].name = name

	def __cinit__(self, object name, unsigned int vendor_id, unsigned int product_id):
		self._JoystickIdentification = <sfJoystickIdentification*> malloc(sizeof(sfJoystickIdentification))
		self.init((name+"\0").encode("ascii"))
		self._JoystickIdentification[0].vendorId = vendor_id
		self._JoystickIdentification[0].productId = product_id

	def __dealloc__(self):
		free(self._JoystickIdentification)

	property name:
		def __get__(self):
			return self._JoystickIdentification[0].name.decode('ascii').rstrip("\0")

		def __set__(self, object name):
			self.init((name+"\0").encode("ascii"))

	property vendor_id:
		def __get__(self):
			return self._JoystickIdentification[0].vendorId

		def __set__(self, unsigned int value):
			self._JoystickIdentification[0].vendorId = value

	property product_id:
		def __get__(self):
			return self._JoystickIdentification[0].productId

		def __set__(self, unsigned int value):
			self._JoystickIdentification[0].productId = value

	def __repr__(self):
		return "JoystickIdentification(\"{0}\", {1}, {2})".format(self.name, self.vendor_id, self.product_id)


cdef class JoystickAxis:
	X = 0
	Y = 1
	Z = 2
	R = 3
	U = 4
	V = 5
	POV_X = 6
	POV_Y = 7


cdef class Joystick:

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def is_connected(unsigned int joystick):
		return sfJoystick_isConnected(joystick)

	@staticmethod
	def get_button_count(unsigned int joystick):
		return sfJoystick_getButtonCount(joystick)

	@staticmethod
	def has_axis(unsigned int joystick, unsigned int axis):
		return sfJoystick_hasAxis(joystick, <sfJoystickAxis>axis)

	@staticmethod
	def is_button_pressed(unsigned int joystick, unsigned int button):
		return sfJoystick_isButtonPressed(joystick, button)

	@staticmethod
	def get_axis_position(unsigned int joystick, unsigned int axis):
		return sfJoystick_getAxisPosition(joystick, <sfJoystickAxis>axis)

	@staticmethod
	def get_identification(unsigned int joystick):
		cdef sfJoystickIdentification joystick_identification = sfJoystick_getIdentification(joystick)
		return JoystickIdentification(joystick_identification.name.decode('ascii'), joystick_identification.vendorId, joystick_identification.productId)

	@staticmethod
	def update():
		sfJoystick_update()
		return

cdef class KeyCode:
	UNKNOWN = -1
	A = 0
	B = 1
	C = 2
	D = 3
	E = 4
	F = 5
	G = 6
	H = 7
	I = 8
	J = 9
	K = 10
	L = 11
	M = 12
	N = 13
	O = 14
	P = 15
	Q = 16
	R = 17
	S = 18
	T = 19
	U = 20
	V = 21
	W = 22
	X = 23
	Y = 24
	Z = 25
	NUM0 = 26
	NUM1 = 27
	NUM2 = 28
	NUM3 = 29
	NUM4 = 30
	NUM5 = 31
	NUM6 = 32
	NUM7 = 33
	NUM8 = 34
	NUM9 = 35
	ESCAPE = 36
	L_CONTROL = 37
	L_SHIFT = 38
	L_ALT = 39
	L_SYSTEM = 40
	R_CONTROL = 41
	R_SHIFT = 42
	R_ALT = 43
	R_SYSTEM = 44
	MENU = 45
	L_BRACKET = 46
	R_BRACKET = 47
	SEMI_COLON = 48
	COMMA = 49
	PERIOD = 50
	QUOTE = 51
	SLASH = 52
	BACK_SLASH = 53
	TILDE = 54
	EQUAL = 55
	DASH = 56
	SPACE = 57
	RETURN = 58
	BACK_SPACE = 59
	TAB = 60
	PAGE_UP = 61
	PAGE_DOWN = 62
	END = 63
	HOME = 64
	INSERT = 65
	DELETE = 66
	ADD = 67
	SUBTRACT = 68
	MULTIPLY = 69
	DIVIDE = 70
	LEFT = 71
	RIGHT = 72
	UP = 73
	DOWN = 74
	NUMPAD0 = 75
	NUMPAD1 = 76
	NUMPAD2 = 77
	NUMPAD3 = 78
	NUMPAD4 = 79
	NUMPAD5 = 80
	NUMPAD6 = 81
	NUMPAD7 = 82
	NUMPAD8 = 83
	NUMPAD9 = 84
	F1 = 85
	F2 = 86
	F3 = 87
	F4 = 88
	F5 = 89
	F6 = 90
	F7 = 91
	F8 = 92
	F9 = 93
	F10 = 94
	F11 = 95
	F12 = 96
	F13 = 97
	F14 = 98
	F15 = 99
	PAUSE = 100
	COUNT = 101


cdef class Keyboard:

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def is_key_pressed(int key):
		return sfKeyboard_isKeyPressed(<sfKeyCode>key)

	@staticmethod
	def set_virtual_keyboard_visible(bint visible):
		sfKeyboard_setVirtualKeyboardVisible(visible)


cdef class MouseButton:
	LEFT = 0
	RIGHT = 1
	MIDDLE = 2
	XBUTTON1 = 3
	XBUTTON2 = 4
	COUNT = 5


cdef class MouseWheel:
	VERTICAL = 0
	HORIZONTAL = 1


cdef class Mouse:

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def is_button_pressed(unsigned int button):
		return sfMouse_isButtonPressed(<sfMouseButton>button)

	@staticmethod
	cdef get_position_window(Window relative_to):
		cdef sfVector2i vector2i
		vector2i = sfMouse_getPosition(relative_to._Window)
		return Vector2i(vector2i.x, vector2i.y)

	@staticmethod
	cdef get_position_render_window(RenderWindow relative_to):
		cdef sfVector2i vector2i
		vector2i = sfMouse_getPosition(<sfWindow*>relative_to._RenderWindow)
		return Vector2i(vector2i.x, vector2i.y)

	@staticmethod
	cdef get_position_null():
		cdef sfVector2i vector2i
		vector2i = sfMouse_getPosition(NULL)
		return Vector2i(vector2i.x, vector2i.y)

	@staticmethod
	def get_position(object relative_to=None):
		if isinstance(relative_to, Window):
			return Mouse.get_position_window(relative_to)
		elif isinstance(relative_to, RenderWindow):
			return Mouse.get_position_render_window(relative_to)
		elif relative_to is None:
			return Mouse.get_position_null()

	@staticmethod
	cdef set_position_window(Vector2i position, Window relative_to):
		sfMouse_setPosition(position._Vector2i[0], relative_to._Window)

	@staticmethod
	cdef set_position_render_window(Vector2i position, RenderWindow relative_to):
		sfMouse_setPosition(position._Vector2i[0], <sfWindow*>relative_to._RenderWindow)

	@staticmethod
	def set_position(Vector2i position, object relative_to=None):
		if isinstance(relative_to, Window):
			Mouse.set_position_window(position, relative_to)
		elif isinstance(relative_to, RenderWindow):
			Mouse.set_position_render_window(position, relative_to)
		elif relative_to is None:
			sfMouse_setPosition(position, NULL)


cdef class Touch:

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def is_down(unsigned int finger):
		return sfTouch_isDown(finger)

	@staticmethod
	def get_position(unsigned int finger, Window relative_to=None):
		cdef sfVector2i vector2i
		if relative_to is None:
			vector2i = sfTouch_getPosition(finger, NULL)
		else:
			vector2i = sfTouch_getPosition(finger, relative_to._Window)
		return Vector2i(vector2i.x, vector2i.y)


cdef class SensorType:
	ACCELEROMETER = 0
	GYROSCOPE = 1
	MAGNETOMETER = 2
	GRAVITY = 3
	USERACCELERATION = 4
	ORIENTATION = 5
	COUNT = 6


cdef class Sensor:

	def __init__(self):
		raise NotImplementedError

	@staticmethod
	def is_available(unsigned int sensor):
		return sfSensor_isAvailable(<sfSensorType>sensor)

	@staticmethod
	def set_enabled(unsigned int sensor, bint enabled):
		return sfSensor_setEnabled(<sfSensorType>sensor, enabled)

	@staticmethod
	def get_value(unsigned int sensor):
		cdef sfVector3f vector3f = sfSensor_getValue(<sfSensorType>sensor)
		return Vector3f(vector3f.x, vector3f.y, vector3f.z)


cdef class EventType:
	CLOSED = 0
	RESIZED = 1
	LOST_FOCUS = 2
	GAINED_FOCUS = 3
	TEXT_ENTERED = 4
	KEY_PRESSED = 5
	KEY_RELEASED = 6
	MOUSE_WHEEL_MOVED = 7
	MOUSE_WHEEL_SCROLLED = 8
	MOUSE_BUTTON_PRESSED = 9
	MOUSE_BUTTON_RELEASED = 10
	MOUSE_MOVED = 11
	MOUSE_ENTERED = 12
	MOUSE_LEFT = 13
	JOYSTICK_BUTTON_PRESSED = 14
	JOYSTICK_BUTTON_RELEASED = 15
	JOYSTICK_MOVED = 16
	JOYSTICK_CONNECTED = 17
	JOYSTICK_DISCONNECTED = 18
	TOUCH_BEGAN = 19
	TOUCH_MOVED = 20
	TOUCH_ENDED = 21
	SENSOR_CHANGED = 22
	COUNT = 23


cdef class Event:

	cdef sfEvent *_Event

	def __cinit__(self):
		self._Event = <sfEvent*> malloc(sizeof(sfEvent))

	def __dealloc__(self):
		if self._Event != NULL:
			free(self._Event)

	property type:
		def __get__(self):
			return self._Event[0].type

	property code:
		def __get__(self):
			if self.type == EventType.KEY_PRESSED or self.type == EventType.KEY_RELEASED:
				return self._Event[0].key.code

	property alt:
		def __get__(self):
			if self.type == EventType.KEY_PRESSED or self.type == EventType.KEY_RELEASED:
				return self._Event[0].key.alt

	property control:
		def __get__(self):
			if self.type == EventType.KEY_PRESSED or self.type == EventType.KEY_RELEASED:
				return self._Event[0].key.control

	property shift:
		def __get__(self):
			if self.type == EventType.KEY_PRESSED or self.type == EventType.KEY_RELEASED:
				return self._Event[0].key.shift

	property system:
		def __get__(self):
			if self.type == EventType.KEY_PRESSED or self.type == EventType.KEY_RELEASED:
				return self._Event[0].key.system

	property unicode:
		def __get__(self):
			if self.type == EventType.TEXT_ENTERED:
				return chr(self._Event[0].text.unicode)

	property x:
		def __get__(self):
			cdef unsigned int type = self.type
			if type == EventType.MOUSE_MOVED:
				return self._Event[0].mouseMove.x
			elif type == EventType.MOUSE_BUTTON_PRESSED or type == EventType.MOUSE_BUTTON_RELEASED:
				return self._Event[0].mouseButton.x
			elif type == EventType.MOUSE_WHEEL_MOVED:
				return self._Event[0].mouseWheel.x
			elif type == EventType.MOUSE_WHEEL_SCROLLED:
				return self._Event[0].mouseWheelScroll.x
			elif type == EventType.TOUCH_BEGAN or type == EventType.TOUCH_MOVED or type == EventType.TOUCH_ENDED:
				return self._Event[0].touch.x
			elif type == EventType.SENSOR_CHANGED:
				return self._Event[0].sensor.x

	property y:
		def __get__(self):
			cdef unsigned int type = self.type
			if type == EventType.MOUSE_MOVED:
				return self._Event[0].mouseMove.y
			elif type == EventType.MOUSE_BUTTON_PRESSED or type == EventType.MOUSE_BUTTON_RELEASED:
				return self._Event[0].mouseButton.y
			elif type == EventType.MOUSE_WHEEL_MOVED:
				return self._Event[0].mouseWheel.y
			elif type == EventType.MOUSE_WHEEL_SCROLLED:
				return self._Event[0].mouseWheelScroll.y
			elif type == EventType.TOUCH_BEGAN or type == EventType.TOUCH_MOVED or type == EventType.TOUCH_ENDED:
				return self._Event[0].touch.y
			elif type == EventType.SENSOR_CHANGED:
				return self._Event[0].sensor.y

	property z:
		def __get__(self):
			if self.type == EventType.SENSOR_CHANGED:
				return self._Event[0].sensor.z

	property button:
		def __get__(self):
			if self.type == EventType.MOUSE_BUTTON_PRESSED or self.type == EventType.MOUSE_BUTTON_RELEASED:
				return self._Event[0].mouseButton.button
			elif self.type == EventType.JOYSTICK_BUTTON_PRESSED or self.type == EventType.JOYSTICK_BUTTON_PRESSED:
				return self._Event[0].joystickButton.button

	property delta:
		def __get__(self):
			if self.type  == EventType.MOUSE_WHEEL_MOVED:
				return self._Event[0].mouseWheel.delta
			elif self.type == EventType.MOUSE_WHEEL_SCROLLED:
				return self._Event[0].mouseWheelScroll.delta

	property joystick_id:
		def __get__(self):
			if self.type == EventType.JOYSTICK_MOVED:
				return self._Event[0].joystickMove.joystickId
			elif self.type == EventType.JOYSTICK_BUTTON_PRESSED or self.type == EventType.JOYSTICK_BUTTON_RELEASED:
				return self._Event[0].joystickButton.joystickId
			elif self.type == EventType.JOYSTICK_CONNECTED or self.type == EventType.JOYSTICK_DISCONNECTED:
				return self._Event[0].joystickConnect.joystickId

	property finger:
		def __get__(self):
			if self.type == EventType.TOUCH_BEGAN or self.type == EventType.TOUCH_MOVED or self.type == EventType.TOUCH_ENDED:
				return self._Event[0].touch.finger

	property sensor_type:
		def __get__(self):
			if self.type == EventType.SENSOR_CHANGED:
				return self._Event[0].sensor.sensorType


cdef class VideoMode:

	cdef sfVideoMode* _VideoMode

	def __cinit__(self, unsigned int width, unsigned int height, unsigned int bits_per_pixel=32):
		self._VideoMode = <sfVideoMode*> malloc(sizeof(sfVideoMode))
		self._VideoMode[0].width = width
		self._VideoMode[0].height = height
		self._VideoMode[0].bitsPerPixel = bits_per_pixel

	def __dealloc__(self):
		free(self._VideoMode)

	property width:
		def __get__(self):
			return self._VideoMode[0].width

		def __set__(self, unsigned int value):
			self._VideoMode[0].width = value

	property height:
		def __get__(self):
			return self._VideoMode[0].height

		def __set__(self, unsigned int value):
			self._VideoMode[0].height = value

	property bits_per_pixel:
		def __get__(self):
			return self._VideoMode[0].bitsPerPixel

		def __set__(self, unsigned int value):
			self._VideoMode[0].bitsPerPixel = value

	cpdef bint is_valid(self):
		return sfVideoMode_isValid(self._VideoMode[0])

	def __repr__(self):
		return "VideoMode({0}, {1}, {2})".format(self.width, self.height, self.bits_per_pixel)


cdef class WindowStyle:
	NONE = 0
	TITLEBAR = 1 << 0
	RESIZE = 1 << 1
	CLOSE = 1 << 2
	FULLSCREEN = 1 << 3
	DEFAULT = TITLEBAR | RESIZE | CLOSE


cdef class ContextAttribute:
	DEFAULT = 0
	CORE = 1 << 0
	DEBUG = 1 << 2


cdef class ContextSettings:

	cdef sfContextSettings *_ContextSettings

	def __cinit__(self, unsigned int depth_bits=0, unsigned int stencil_bits=0, unsigned int antialiasing_level=0, unsigned int major_version=1, unsigned int minor_version=1, unsigned int attribute_flags=ContextAttribute.DEFAULT, bint s_rgb_capable=False):
		self._ContextSettings = <sfContextSettings*> malloc(sizeof(sfContextSettings))
		self._ContextSettings[0].depthBits = depth_bits
		self._ContextSettings[0].stencilBits= stencil_bits
		self._ContextSettings[0].antialiasingLevel = antialiasing_level
		self._ContextSettings[0].majorVersion = major_version
		self._ContextSettings[0].minorVersion = minor_version
		self._ContextSettings[0].attributeFlags = attribute_flags
		self._ContextSettings[0].sRgbCapable = s_rgb_capable

	def __dealloc__(self):
		free(self._ContextSettings)

	property depth_bits:
		def __get__(self):
			return self._ContextSettings[0].depthBits

		def __set__(self, unsigned int value):
			self._ContextSettings[0].depthBits = value

	property stencil_bits:
		def __get__(self):
			return self._ContextSettings[0].stencilBits

		def __set__(self, unsigned int value):
			self._ContextSettings[0].stencilBits = value

	property antialiasing_level:
		def __get__(self):
			return self._ContextSettings[0].antialiasingLevel

		def __set__(self, unsigned int value):
			self._ContextSettings[0].antialiasingLevel = value

	property major_version:
		def __get__(self):
			return self._ContextSettings[0].majorVersion

		def __set__(self, unsigned int value):
			self._ContextSettings[0].majorVersion = value

	property minor_version:
		def __get__(self):
			return self._ContextSettings[0].minorVersion

		def __set__(self, unsigned int value):
			self._ContextSettings[0].minorVersion = value

	property attribute_flags:
		def __get__(self):
			return self._ContextSettings[0].attributeFlags

		def __set__(self, unsigned int value):
			self._ContextSettings[0].attributeFlags = value

	property s_rgb_capable:
		def __get__(self):
			return self._ContextSettings[0].sRgbCapable

		def __set__(self, bint value):
			self._ContextSettings[0].sRgbCapable = value

	def __repr__(self):
		return "ContextSettings({0}, {1}, {2}, {3}, {4}, {5}, {6})".format(self.depth_bits, self.stencil_bits, self.antialiasing_level, self.major_version, self.minor_version, self.attribute_flags, self.s_rgb_capable)


cdef class Window:

	cdef sfWindow* _Window

	cdef init(self, VideoMode mode, char* title, unsigned int style, ContextSettings settings):
		if settings is None:
			self._Window = sfWindow_createUnicode(mode._VideoMode[0], <unsigned int*>title, style, NULL)
		else:
			self._Window = sfWindow_createUnicode(mode._VideoMode[0], <unsigned int*>title, style, settings._ContextSettings)

	def __cinit__(self, VideoMode mode, object title, unsigned int style=WindowStyle.DEFAULT, ContextSettings settings=None):
		self.init(mode, (title+"\0").encode("utf_32"), style, settings)

	def __dealloc__(self):
		sfWindow_destroy(self._Window)

	cpdef close(self):
		sfWindow_close(self._Window)

	cpdef bint is_open(self):
		sfWindow_isOpen(self._Window)

	property context_settings:
		def __get__(self):
			cdef sfContextSettings settings = sfWindow_getSettings(self._Window)
			return ContextSettings(settings.depthBits, settings.stencilBits, settings.antialiasingLevel, settings.majorVersion, settings.minorVersion, settings.attributeFlags, settings.sRgbCapable)

	cpdef poll_event(self):
		cdef Event event = Event()
		if sfWindow_pollEvent(self._Window, event._Event):
			return event
		del event

	cpdef wait_event(self):
		cdef Event event = Event()
		if sfWindow_waitEvent(self._Window, event._Event):
			return event
		del event

	def events(self):
		cdef Event event = Event()
		while sfWindow_pollEvent(self._Window, event._Event):
			yield event
		del event

	property position:
		def __get__(self):
			cdef sfVector2i position = sfWindow_getPosition(self._Window)
			return Vector2i(position.x, position.y)

		def __set__(self, Vector2i position):
			sfWindow_setPosition(self._Window, position._Vector2i[0])

	property size:
		def __get__(self):
			cdef sfVector2u size = sfWindow_getSize(self._Window)
			return Vector2u(size.x, size.y)

		def __set__(self, Vector2u size):
			sfWindow_setSize(self._Window, size._Vector2u[0])

	property title:
		def __set__(self, char* title):
			sfWindow_setUnicodeTitle(self._Window, <unsigned int*>title)

	cpdef set_icon(self, unsigned int width, unsigned int height, char* pixels):
		sfWindow_setIcon(self._Window, width, height, <const unsigned char*>pixels)

	property visible:
		def __set__(self, bint visible):
			sfWindow_setVisible(self._Window, visible)

	property vertical_sync_enabled:
		def __set__(self, bint enabled):
			sfWindow_setVerticalSyncEnabled(self._Window, enabled)

	property mouse_cursor_visible:
		def __set__(self, bint visible):
			sfWindow_setMouseCursorVisible(self._Window, visible)

	property mouse_cursor_grabbed:
		def __set__(self, bint grabbed):
			sfWindow_setMouseCursorGrabbed(self._Window, grabbed)

	property key_repeat_enabled:
		def __set__(self, bint enabled):
			sfWindow_setKeyRepeatEnabled(self._Window, enabled)

	property framerate_limit:
		def __set__(self, unsigned int limit):
			sfWindow_setFramerateLimit(self._Window, limit)

	property joystick_threshold:
		def __set__(self, float threshold):
			sfWindow_setJoystickThreshold(self._Window, threshold)

	property active:
		def __set__(self, bint active):
			sfWindow_setActive(self._Window, active)

	cpdef request_focus(self):
		sfWindow_requestFocus(self._Window)

	cpdef bint has_focus(self):
		return sfWindow_hasFocus(self._Window)

	cpdef display(self):
		sfWindow_display(self._Window)


cdef class Color:

	cdef sfColor *_Color

	def __cinit__(self, unsigned char red=0, unsigned char green=0, unsigned char blue=0, unsigned char alpha=255):
		self._Color = <sfColor*> malloc(sizeof(sfColor))
		self._Color[0] = sfColor_fromRGBA(red, green, blue, alpha)

	def __dealloc__(self):
		free(self._Color)

	property r:
		def __get__(self):
			return self._Color[0].r

		def __set__(self, unsigned char value):
			self._Color[0].r = value

	property g:
		def __get__(self):
			return self._Color[0].g

		def __set__(self, unsigned char value):
			self._Color[0].g = value

	property b:
		def __get__(self):
			return self._Color[0].b

		def __set__(self, unsigned char value):
			self._Color[0].b = value

	property a:
		def __get__(self):
			return self._Color[0].a

		def __set__(self, unsigned char value):
			self._Color[0].a = value

	def __add__(self, Color other):
		return Color(self.r + other.r, self.g + other.g, self.b + other.b, self.a + other.a)

	def __repr__(self):
		return "Color({0}, {1}, {2}, {3})".format(self.r, self.g, self.b, self.a)


cdef class FloatRect:

	cdef sfFloatRect *_FloatRect

	def __cinit__(self, float left=0., float top=0., float width=0., float height=0.):
		self._FloatRect = <sfFloatRect*> malloc(sizeof(sfFloatRect))
		self._FloatRect[0].left = left
		self._FloatRect[0].top = top
		self._FloatRect[0].width = width
		self._FloatRect[0].height = height

	def __dealloc__(self):
		free(self._FloatRect)

	cpdef bint contains(self, Vector2f point):
		return sfFloatRect_contains(self._FloatRect, point.x, point.y)

	cpdef bint intersects(self, FloatRect other):
		cdef FloatRect intersection = FloatRect()
		return sfFloatRect_intersects(self._FloatRect, other._FloatRect, intersection._FloatRect)

	property left:
		def __get__(self):
			return self._FloatRect[0].left

		def __set__(self, float value):
			self._FloatRect[0].left = value

	property top:
		def __get__(self):
			return self._FloatRect[0].top

		def __set__(self, float value):
			self._FloatRect[0].top = value

	property width:
		def __get__(self):
			return self._FloatRect[0].width

		def __set__(self, float value):
			self._FloatRect[0].width = value

	property height:
		def __get__(self):
			return self._FloatRect[0].height

		def __set__(self, float value):
			self._FloatRect[0].height = value

	def __repr__(self):
		return "FloatRect({0}, {1}, {2}, {3})".format(self.left, self.top, self.width, self.height)


cdef class IntRect:

	cdef sfIntRect *_IntRect

	def __cinit__(self, int left=0, int top=0, int width=0, int height=0):
		self._IntRect = <sfIntRect*> malloc(sizeof(sfIntRect))
		self._IntRect[0].left = left
		self._IntRect[0].top = top
		self._IntRect[0].width = width
		self._IntRect[0].height = height

	def __dealloc__(self):
		free(self._IntRect)

	cpdef bint contains(self, Vector2i point):
		return sfIntRect_contains(self._IntRect, point.x, point.y)

	cpdef bint intersects(self, IntRect other):
		cdef IntRect intersection = IntRect()
		return sfIntRect_intersects(self._IntRect, other._IntRect, intersection._IntRect)

	property left:
		def __get__(self):
			return self._IntRect[0].left

		def __set__(self, int value):
			self._IntRect[0].left = value

	property top:
		def __get__(self):
			return self._IntRect[0].top

		def __set__(self, int value):
			self._IntRect[0].top = value

	property width:
		def __get__(self):
			return self._IntRect[0].width

		def __set__(self, int value):
			self._IntRect[0].width = value

	property height:
		def __get__(self):
			return self._IntRect[0].height

		def __set__(self, int value):
			self._IntRect[0].height = value

	def __repr__(self):
		return "IntRect({0}, {1}, {2}, {3})".format(self.left, self.top, self.width, self.height)


cdef class Shader:

	cdef sfShader *_Shader

	def __dealloc__(self):
		if self._Shader is not NULL:
			sfShader_destroy(self._Shader)

	@staticmethod
	cdef create_from_file(char* vertex_shader_filename, char* geometry_shader_filename, char* fragment_shader_filename):
		cdef Shader shader = Shader()
		shader._Shader = sfShader_createFromFile(vertex_shader_filename, geometry_shader_filename, fragment_shader_filename)
		return shader

	@staticmethod
	def from_file(object vertex_shader_filename, object geometry_shader_filename, object fragment_shader_filename):
		return Shader.create_from_file((vertex_shader_filename+"\0").encode('ascii'), (geometry_shader_filename+"\0").encode('ascii'), (fragment_shader_filename+"\0").encode('ascii'))

	@staticmethod
	def is_available():
		return sfShader_isAvailable()


cdef class BlendFactor:
	ZERO = 0
	ONE = 1
	SRC_COLOR = 2
	ONE_MINUS_SRC_COLOR = 3
	DST_COLOR = 4
	ONE_MINUS_DST_COLOR = 5
	SRC_ALPHA = 6
	ONE_MINUS_SRC_ALPHA = 7
	DST_ALPHA = 8
	ONE_MINUS_DST_ALPHA = 9


cdef class BlendEquation:
	ADD = 0
	SUBTRACT = 1
	REVERSE_SUBSTRACT = 2


cdef class BlendMode:

	cdef sfBlendMode *_BlendMode

	def __cinit__(self, int color_source_factor, int color_destination_factor, int color_blend_equation=BlendEquation.ADD, int alpha_source_factor=-1, int alpha_destination_factor=-1, int alpha_blend_equation=-1):
		self._BlendMode = <sfBlendMode*> malloc(sizeof(sfBlendMode))
		if color_source_factor == -1:
			self._BlendMode[0].colorSrcFactor = self._BlendMode[0].colorDstFactor = <sfBlendFactor>color_source_factor
			self._BlendMode[0].alphaSrcFactor = self._BlendMode[0].alphaDstFactor = <sfBlendFactor>color_destination_factor
			self._BlendMode[0].colorEquation = self._BlendMode[0].alphaEquation = <sfBlendEquation>color_blend_equation
		else:
			self._BlendMode[0].colorSrcFactor = <sfBlendFactor>color_source_factor
			self._BlendMode[0].colorDstFactor = <sfBlendFactor>color_destination_factor
			self._BlendMode[0].colorEquation = <sfBlendEquation>color_blend_equation
			self._BlendMode[0].alphaSrcFactor = <sfBlendFactor>alpha_source_factor
			self._BlendMode[0].alphaDstFactor = <sfBlendFactor>alpha_destination_factor
			self._BlendMode[0].alphaEquation = <sfBlendEquation>alpha_blend_equation

	def __dealloc__(self):
		free(self._BlendMode)

	property color_source_factor:
		def __get__(self):
			return self._BlendMode[0].colorSrcFactor

		def __set__(self, unsigned int value):
			self._BlendMode[0].colorSrcFactor = <sfBlendFactor>value

	property color_destination_factor:
		def __get__(self):
			return self._BlendMode[0].colorDstFactor

		def __set__(self, unsigned int value):
			self._BlendMode[0].colorDstFactor = <sfBlendFactor>value

	property color_blend_equation:
		def __get__(self):
			return self._BlendMode[0].colorEquation

		def __set__(self, unsigned int value):
			self._BlendMode[0].colorEquation = <sfBlendEquation>value

	property alpha_source_factor:
		def __get__(self):
			return self._BlendMode[0].alphaSrcFactor

		def __set__(self, unsigned int value):
			self._BlendMode[0].alphaSrcFactor = <sfBlendFactor>value

	property alpha_destination_factor:
		def __get__(self):
			return self._BlendMode[0].alphaDstFactor

		def __set__(self, unsigned int value):
			self._BlendMode[0].alphaDstFactor = <sfBlendFactor>value

	property alpha_blend_equation:
		def __get__(self):
			return self._BlendMode[0].alphaEquation

		def __set__(self, unsigned int value):
			self._BlendMode[0].alphaEquation = <sfBlendEquation>value

BlendAlpha = BlendMode(BlendFactor.SRC_ALPHA, BlendFactor.ONE_MINUS_SRC_ALPHA, BlendEquation.ADD, BlendFactor.ONE, BlendFactor.ONE_MINUS_SRC_ALPHA, BlendEquation.ADD)
BlendAdd = BlendMode(BlendFactor.SRC_ALPHA, BlendFactor.ONE, BlendEquation.ADD, BlendFactor.ONE, BlendFactor.ONE, BlendEquation.ADD)
BlendMultiply = BlendMode(BlendFactor.DST_COLOR, BlendFactor.ZERO)
BlendNone = BlendMode(BlendFactor.ONE, BlendFactor.ZERO)


cdef class Transform:

	cdef sfTransform *_Transform

	def __cinit__(self, float a00, float a01, float a02, float a10, float a11, float a12, float a20, float a21, float a22, bint init=True):
		self._Transform = <sfTransform*> malloc(sizeof(sfTransform))
		if init:
			self._Transform[0] = sfTransform_fromMatrix(a00, a01, a02, a10, a11, a12, a20, a21, a22)

	def __dealloc__(self):
		free(self._Transform)

	cpdef Transform inverse(self):
		cdef Transform transform = Transform(0., 0., 0., 0., 0., 0., 0., 0., 0., init=False)
		transform._Transform[0] = sfTransform_getInverse(self._Transform)
		return transform

	cpdef Vector2f transform_point(self, Vector2f point):
		cdef sfVector2f res = sfTransform_transformPoint(self._Transform, point._Vector2f[0])
		return Vector2f(res.x, res.y)

	cpdef FloatRect transform_rect(self, FloatRect rectangle):
		cdef sfFloatRect res = sfTransform_transformRect(self._Transform, rectangle._FloatRect[0])
		return FloatRect(res.left, res.top, res.width, res.height)

	cpdef combine(self, Transform other):
		sfTransform_combine(self._Transform, other._Transform)

	def translate(self, float x, float y):
		sfTransform_translate(self._Transform, x, y)

	def rotate(self, float angle):
		sfTransform_rotate(self._Transform, angle)

	def rotate_with_center(self, float angle, float center_x, float center_y):
		sfTransform_rotateWithCenter(self._Transform, angle, center_x, center_y)

	def scale(self, float scale_x, float scale_y):
		sfTransform_scale(self._Transform, scale_x, scale_y)

	def scale_with_center(self, float scale_x, float scale_y, float center_x, float center_y):
		sfTransform_scaleWithCenter(self._Transform, scale_x, scale_y, center_x, center_y)

TransformIdentity = Transform(1., 0., 0., 0., 1., 0., 0., 0., 1.)


cdef class Image:

	cdef sfImage *_Image

	def __cinit__(self, unsigned int width, unsigned int height, Color color=None, bint init=True):
		if init:
			if color is None:
				self._Image = sfImage_create(width, height)
			else:
				self._Image = sfImage_createFromColor(width, height, color._Color[0])

	def __dealloc__(self):
		if self._Image is not NULL:
			sfImage_destroy(self._Image)

	@staticmethod
	def create_from_file(char* filename):
		cdef Image image = Image(0, 0, init=False)
		image._Image = sfImage_createFromFile(filename)
		return image

	@staticmethod
	def create_from_memory(char* ptr, int size):
		cdef Image image = Image(0, 0, init=False)
		image._Image = sfImage_createFromMemory(ptr, <size_t>size)
		return image

	@staticmethod
	def from_file(object filename):
		cdef Image image = Image.create_from_file(filename.encode())
		if image._Image is not NULL:
			return image

	@staticmethod
	def from_bytes(object ptr):
		cdef Image image = Image.create_from_memory(ptr, len(ptr))
		if image._Image is not NULL:
			return image

	def __copy__(self):
		cdef Image image = Image(0, 0, init=False)
		image._Image = sfImage_copy(self._Image)
		return image

	cdef bint save_to_file(self, char* filename):
		return sfImage_saveToFile(self._Image, filename)

	cpdef bint save(self, object filename):
		return self.save_to_file(filename.encode())

	property size:
		def __get__(self):
			cdef sfVector2u size = sfImage_getSize(self._Image)
			return Vector2u(size.x, size.y)

	cpdef void set_pixel(self, unsigned int x, unsigned int y, Color color):
		sfImage_setPixel(self._Image, x, y, color._Color[0])

	cpdef Color get_pixel(self, unsigned int x, unsigned int y):
		cdef sfColor color = sfImage_getPixel(self._Image, x, y)
		return Color(color.r, color.g, color.b, color.a)

	cpdef void flip_horizontally(self):
		sfImage_flipHorizontally(self._Image)

	cpdef void flip_vertically(self):
		sfImage_flipVertically(self._Image)


cdef class Texture:

	cdef sfTexture *_Texture

	def __cinit__(self, unsigned int width, unsigned int height, bint init=True):
		if init:
			self._Texture = sfTexture_create(width, height)

	def __dealloc__(self):
		if self._Texture is not NULL:
			sfTexture_destroy(self._Texture)

	@staticmethod
	def create_from_file(char* filename, IntRect area):
		cdef Texture texture = Texture(0, 0, init=False)
		if area is None:
			texture._Texture = sfTexture_createFromFile(filename, NULL)
		else:
			texture._Texture = sfTexture_createFromFile(filename, area._IntRect)
		return texture

	@staticmethod
	def create_from_memory(char* ptr, int size, IntRect area):
		cdef Texture texture = Texture(0, 0, init=False)
		if area is None:
			texture._Texture = sfTexture_createFromMemory(ptr, <size_t>size, NULL)
		else:
			texture._Texture = sfTexture_createFromMemory(ptr, <size_t>size, area._IntRect)
		return texture

	@staticmethod
	def from_file(object filename, IntRect area=None):
		cdef Texture texture = Texture.create_from_file((filename+"\0").encode('ascii'), area)
		if texture._Texture is not NULL:
			return texture

	@staticmethod
	def from_bytes(object ptr, IntRect area=None):
		cdef Texture texture = Texture.create_from_memory(ptr, len(ptr), area)
		if texture._Texture is not NULL:
			return texture

	@staticmethod
	def from_image(Image image, IntRect area=None):
		cdef Texture texture = Texture(0, 0, init=False)
		if area is None:
			texture._Texture = sfTexture_createFromImage(image._Image, NULL)
		else:
			texture._Texture = sfTexture_createFromImage(image._Image, area._IntRect)
		return texture

	def __copy__(self):
		cdef Texture texture = Texture(0, 0, init=False)
		texture._Texture = sfTexture_copy(self._Texture)
		return Texture

	property smooth:
		def __get__(self):
			return sfTexture_isSmooth(self._Texture)

		def __set__(self, bint smooth):
			sfTexture_setSmooth(self._Texture, smooth)

	property s_rgb:
		def __get__(self):
			return sfTexture_isSrgb(self._Texture)

		def __set__(self, bint s_rgb):
			sfTexture_setSrgb(self._Texture, s_rgb)

	property repeated:
		def __get__(self):
			return sfTexture_isRepeated(self._Texture)

		def __set__(self, bint repeated):
			sfTexture_setRepeated(self._Texture, repeated)


cdef class RectangleShape:

	cdef sfRectangleShape *_RectangleShape

	def __cinit__(self, Vector2f size=None, bint init=True):
		if init:
			self._RectangleShape = sfRectangleShape_create()

	def __init__(self, Vector2f size=None, bint init=True):
		if init and size is not None:
			self.size = size

	def __dealloc__(self):
		if self._RectangleShape is not NULL:
			sfRectangleShape_destroy(self._RectangleShape)

	def __copy__(self):
		cdef RectangleShape rectangle_shape = RectangleShape(init=False)
		rectangle_shape._RectangleShape = sfRectangleShape_copy(self._RectangleShape)
		return rectangle_shape

	property position:
		def __get__(self):
			cdef sfVector2f position = sfRectangleShape_getPosition(self._RectangleShape)
			return Vector2f(position.x, position.y)

		def __set__(self, Vector2f position):
			sfRectangleShape_setPosition(self._RectangleShape, position._Vector2f[0])

	property rotation:
		def __get__(self):
			return sfRectangleShape_getRotation(self._RectangleShape)

		def __set__(self, float rotation):
			sfRectangleShape_setRotation(self._RectangleShape, rotation)

	property scale:
		def __get__(self):
			cdef sfVector2f scale = sfRectangleShape_getScale(self._RectangleShape)
			return Vector2f(scale.x, scale.y)

		def __set__(self, Vector2f scale):
			sfRectangleShape_setScale(self._RectangleShape, scale._Vector2f[0])

	property origin:
		def __get__(self):
			cdef sfVector2f origin = sfRectangleShape_getOrigin(self._RectangleShape)
			return Vector2f(origin.x, origin.y)

		def __set__(self, Vector2f origin):
			sfRectangleShape_setOrigin(self._RectangleShape, origin._Vector2f[0])

	def move(self, Vector2f offset):
		sfRectangleShape_move(self._RectangleShape, offset._Vector2f[0])

	def rotate(self, float angle):
		sfRectangleShape_rotate(self._RectangleShape, angle)

	def scale_by(self, Vector2f offset):
		sfRectangleShape_scale(self._RectangleShape, Vector2f._Vector2f[0])

	cpdef set_texture(self, Texture texture, bint reset_rect=False):
		sfRectangleShape_setTexture(self._RectangleShape, texture._Texture, reset_rect)

	property fill_color:
		def __get__(self):
			cdef sfColor color = sfRectangleShape_getFillColor(self._RectangleShape)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfRectangleShape_setFillColor(self._RectangleShape, color._Color[0])

	property outline_color:
		def __get__(self):
			cdef sfColor color = sfRectangleShape_getOutlineColor(self._RectangleShape)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfRectangleShape_setOutlineColor(self._RectangleShape, color._Color[0])

	property outline_thickness:
		def __get__(self):
			return sfRectangleShape_getOutlineThickness(self._RectangleShape)

		def __set__(self, float thickness):
			sfRectangleShape_setOutlineThickness(self._RectangleShape, thickness)

	property size:
		def __get__(self):
			cdef sfVector2f size = sfRectangleShape_getSize(self._RectangleShape)
			return Vector2f(size.x, size.y)

		def __set__(self, Vector2f size):
			sfRectangleShape_setSize(self._RectangleShape, size._Vector2f[0])

	property local_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfRectangleShape_getLocalBounds(self._RectangleShape)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)

	property global_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfRectangleShape_getGlobalBounds(self._RectangleShape)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)


cdef class Sprite:

	cdef sfSprite *_Sprite
	# prevent texture from being freed too early
	cdef Texture _Texture

	def __cinit__(self):
		self._Sprite = sfSprite_create()

	def __dealloc__(self):
		sfSprite_destroy(self._Sprite)

	property position:
		def __get__(self):
			cdef sfVector2f position = sfSprite_getPosition(self._Sprite)
			return Vector2f(position.x, position.y)

		def __set__(self, Vector2f position):
			sfSprite_setPosition(self._Sprite, position._Vector2f[0])

	property rotation:
		def __get__(self):
			return sfSprite_getRotation(self._Sprite)

		def __set__(self, float rotation):
			sfSprite_setRotation(self._Sprite, rotation)

	property scale:
		def __get__(self):
			cdef sfVector2f scale = sfSprite_getScale(self._Sprite)
			return Vector2f(scale.x, scale.y)

		def __set__(self, Vector2f scale):
			sfSprite_setScale(self._Sprite, scale._Vector2f[0])

	property origin:
		def __get__(self):
			cdef sfVector2f origin = sfSprite_getOrigin(self._Sprite)
			return Vector2f(origin.x, origin.y)

		def __set__(self, Vector2f origin):
			sfSprite_setOrigin(self._Sprite, origin._Vector2f[0])

	def move(self, Vector2f offset):
		sfSprite_move(self._Sprite, offset._Vector2f[0])

	def rotate(self, float angle):
		sfSprite_rotate(self._Sprite, angle)

	def scale_by(self, Vector2f offset):
		sfSprite_scale(self._Sprite, Vector2f._Vector2f[0])

	cpdef set_texture(self, Texture texture, bint reset_rect=False):
		sfSprite_setTexture(self._Sprite, texture._Texture, reset_rect)

	property color:
		def __get__(self):
			cdef sfColor color = sfSprite_getColor(self._Sprite)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfSprite_setColor(self._Sprite, color._Color[0])

	property texture_rect:
		def __get__(self):
			cdef sfIntRect rect = sfSprite_getTextureRect(self._Sprite)
			return IntRect(rect.left, rect.top, rect.width, rect.height)

	property local_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfSprite_getLocalBounds(self._Sprite)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)

	property global_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfSprite_getGlobalBounds(self._Sprite)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)


cdef class Font:

	cdef sfFont *_Font

	def __dealloc__(self):
		if self._Font is not NULL:
			sfFont_destroy(self._Font)

	@staticmethod
	def create_from_file(char* filename):
		cdef Font font = Font()
		font._Font = sfFont_createFromFile(filename)
		return font

	@staticmethod
	def from_file(object filename):
		return Font.create_from_file((filename+"\0").encode('ascii'))


cdef class TextStyle:
	REGULAR = 0
	BOLD = 1 << 0
	ITALIC = 1 << 1
	UNDERLINED = 1 << 2
	STRIKE_THROUGH = 1 << 3


cdef class Text:

	cdef sfText *_Text
	# prevent font from being freed too early
	cdef Font _Font

	def __cinit__(self, object string=None, Font font=None):
		self._Text = sfText_create()

	def __init__(self, object string=None, Font font=None):
		if string is not None:
			self.string = string
		if font is not None:
			self.font = font

	def __dealloc__(self):
		sfText_destroy(self._Text)

	property position:
		def __get__(self):
			cdef sfVector2f position = sfText_getPosition(self._Text)
			return Vector2f(position.x, position.y)

		def __set__(self, Vector2f position):
			sfText_setPosition(self._Text, position._Vector2f[0])

	property rotation:
		def __get__(self):
			return sfText_getRotation(self._Text)

		def __set__(self, float rotation):
			sfText_setRotation(self._Text, rotation)

	property scale:
		def __get__(self):
			cdef sfVector2f scale = sfText_getScale(self._Text)
			return Vector2f(scale.x, scale.y)

		def __set__(self, Vector2f scale):
			sfText_setScale(self._Text, scale._Vector2f[0])

	property origin:
		def __get__(self):
			cdef sfVector2f origin = sfText_getOrigin(self._Text)
			return Vector2f(origin.x, origin.y)

		def __set__(self, Vector2f origin):
			sfText_setOrigin(self._Text, origin._Vector2f[0])

	def move(self, Vector2f offset):
		sfText_move(self._Text, offset._Vector2f[0])

	def rotate(self, float angle):
		sfText_rotate(self._Text, angle)

	def scale_by(self, Vector2f offset):
		sfText_scale(self._Text, Vector2f._Vector2f[0])

	cdef set_string(self, char* string):
		sfText_setUnicodeString(self._Text, <unsigned int*>string)

	property string:
		def __get__(self):
			cdef object res = ""
			cdef const unsigned int *string = sfText_getUnicodeString(self._Text)
			i = 0
			while string[i]:
				res += chr(string[i])
				i += 1
			return res

		def __set__(self, object string):
			self.set_string((string+"\0").encode("utf_32")[4:])

	property string_bytes:
		def __get__(self):
			return sfText_getString(self._Text)

		def __set__(self, char* string):
			sfText_setString(self._Text, string)

	property font:
		def __get__(self):
			return self._Font

		def __set__(self, Font font):
			sfText_setFont(self._Text, font._Font)
			self._Font = font

	property character_size:
		def __get__(self):
			return sfText_getCharacterSize(self._Text)

		def __set__(self, unsigned int size):
			sfText_setCharacterSize(self._Text, size)

	property style:
		def __get__(self):
			return sfText_getStyle(self._Text)

		def __set__(self, unsigned int style):
			sfText_setStyle(self._Text, style)

	property color:
		def __get__(self):
			cdef sfColor color = sfText_getColor(self._Text)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfText_setColor(self._Text, color._Color[0])

	property fill_color:
		def __get__(self):
			cdef sfColor color = sfText_getFillColor(self._Text)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfText_setFillColor(self._Text, color._Color[0])

	property outline_color:
		def __get__(self):
			cdef sfColor color = sfText_getOutlineColor(self._Text)
			return Color(color.r, color.g, color.b, color.a)

		def __set__(self, Color color):
			sfText_setOutlineColor(self._Text, color._Color[0])

	property outline_thickness:
		def __get__(self):
			return sfText_getOutlineThickness(self._Text)

		def __set__(self, float thickness):
			sfText_setOutlineThickness(self._Text, thickness)

	cpdef Vector2f find_character_pos(self, unsigned int index):
		cdef sfVector2f pos = sfText_findCharacterPos(self._Text, <size_t>index)
		return Vector2f(pos.x, pos.y)

	property local_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfText_getLocalBounds(self._Text)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)

	property global_bounds:
		def __get__(self):
			cdef sfFloatRect bounds = sfText_getGlobalBounds(self._Text)
			return FloatRect(bounds.left, bounds.top, bounds.width, bounds.height)

	def __repr__(self):
		return "Text(\"{0}\")".format(self.string)


cdef class RenderStates:

	cdef sfRenderStates *_RenderStates
	# prevent the fields from being freed too early
	cdef Transform _Transform
	cdef Texture _Texture
	cdef Shader _Shader

	def __cinit__(self, BlendMode blend_mode=BlendAlpha, Transform transform=TransformIdentity, Texture texture=None, Shader shader=None):
		self._RenderStates = <sfRenderStates*> malloc(sizeof(sfRenderStates))
		self._RenderStates[0].blendMode = blend_mode._BlendMode[0]
		self._RenderStates[0].transform = transform._Transform[0]
		self._Transform = transform
		if texture is None:
			self._RenderStates[0].texture = NULL
		else:
			self._RenderStates[0].texture = texture._Texture
		self._Texture = texture
		if shader is None:
			self._RenderStates[0].shader = NULL
		else:
			self._RenderStates[0].shader = shader._Shader
		self._Shader = shader

	def __dealloc__(self):
		free(self._RenderStates)

	property blend_mode:
		def __get__(self):
			cdef sfBlendMode blend_mode = self._RenderStates[0].blendMode
			return BlendMode(blend_mode.colorSrcFactor, blend_mode.colorDstFactor, blend_mode.colorEquation, blend_mode.alphaSrcFactor, blend_mode.alphaDstFactor, blend_mode.alphaEquation)

		def __set__(self, BlendMode blend_mode):
			self._RenderStates[0].blendMode = blend_mode._BlendMode[0]

	property transform:
		def __get__(self):
			return self._Transform

		def __set__(self, Transform transform):
			self._RenderStates[0].transform = transform._Transform[0]
			self._Transform = transform

	property texture:
		def __get__(self):
			return self._Texture


cdef class RenderWindow:

	cdef sfRenderWindow *_RenderWindow

	cdef init(self, VideoMode mode, char* title, unsigned int style, ContextSettings settings):
		if settings is None:
			self._RenderWindow = sfRenderWindow_createUnicode(mode._VideoMode[0], <unsigned int*>title, style, NULL)
		else:
			self._RenderWindow = sfRenderWindow_createUnicode(mode._VideoMode[0], <unsigned int*>title, style, settings._ContextSettings)

	def __cinit__(self, VideoMode mode, object title, unsigned int style=WindowStyle.DEFAULT, ContextSettings settings=None):
		self.init(mode, (title+"\0").encode("utf_32"), style, settings)

	def __dealloc__(self):
		sfRenderWindow_destroy(self._RenderWindow)

	cpdef close(self):
		sfRenderWindow_close(self._RenderWindow)

	cpdef bint is_open(self):
		sfRenderWindow_isOpen(self._RenderWindow)

	property context_settings:
		def __get__(self):
			cdef sfContextSettings settings = sfRenderWindow_getSettings(self._RenderWindow)
			return ContextSettings(settings.depthBits, settings.stencilBits, settings.antialiasingLevel, settings.majorVersion, settings.minorVersion, settings.attributeFlags, settings.sRgbCapable)

	cpdef poll_event(self):
		cdef Event event = Event()
		if sfRenderWindow_pollEvent(self._RenderWindow, event._Event):
			return event
		del event

	cpdef wait_event(self):
		cdef Event event = Event()
		if sfRenderWindow_waitEvent(self._RenderWindow, event._Event):
			return event
		del event

	def events(self):
		cdef Event event = Event()
		while sfRenderWindow_pollEvent(self._RenderWindow, event._Event):
			yield event
		del event

	property position:
		def __get__(self):
			cdef sfVector2i position = sfRenderWindow_getPosition(self._RenderWindow)
			return Vector2i(position.x, position.y)

		def __set__(self, Vector2i position):
			sfRenderWindow_setPosition(self._RenderWindow, position._Vector2i[0])

	property size:
		def __get__(self):
			cdef sfVector2u size = sfRenderWindow_getSize(self._RenderWindow)
			return Vector2u(size.x, size.y)

		def __set__(self, Vector2u size):
			sfRenderWindow_setSize(self._RenderWindow, size._Vector2u[0])

	property title:
		def __set__(self, char* title):
			sfRenderWindow_setUnicodeTitle(self._RenderWindow, <unsigned int*>title)

	cpdef set_icon(self, unsigned int width, unsigned int height, char* pixels):
		sfRenderWindow_setIcon(self._RenderWindow, width, height, <const unsigned char*>pixels)

	property visible:
		def __set__(self, bint visible):
			sfRenderWindow_setVisible(self._RenderWindow, visible)

	property vertical_sync_enabled:
		def __set__(self, bint enabled):
			sfRenderWindow_setVerticalSyncEnabled(self._RenderWindow, enabled)

	property mouse_cursor_visible:
		def __set__(self, bint visible):
			sfRenderWindow_setMouseCursorVisible(self._RenderWindow, visible)

	property mouse_cursor_grabbed:
		def __set__(self, bint grabbed):
			sfRenderWindow_setMouseCursorGrabbed(self._RenderWindow, grabbed)

	property key_repeat_enabled:
		def __set__(self, bint enabled):
			sfRenderWindow_setKeyRepeatEnabled(self._RenderWindow, enabled)

	property framerate_limit:
		def __set__(self, unsigned int limit):
			sfRenderWindow_setFramerateLimit(self._RenderWindow, limit)

	property joystick_threshold:
		def __set__(self, float threshold):
			sfRenderWindow_setJoystickThreshold(self._RenderWindow, threshold)

	property active:
		def __set__(self, bint active):
			sfRenderWindow_setActive(self._RenderWindow, active)

	cpdef request_focus(self):
		sfRenderWindow_requestFocus(self._RenderWindow)

	cpdef bint has_focus(self):
		return sfRenderWindow_hasFocus(self._RenderWindow)

	cpdef display(self):
		sfRenderWindow_display(self._RenderWindow)

	cpdef clear(self, Color color):
		sfRenderWindow_clear(self._RenderWindow, color._Color[0])

	cpdef map_pixel_to_coords(self, Vector2i point):
		cdef sfVector2f coords = sfRenderWindow_mapPixelToCoords(self._RenderWindow, point._Vector2i[0], NULL)
		return Vector2f(coords.x, coords.y)

	cpdef map_coords_to_pixel(self, Vector2f point):
		cdef sfVector2i pixel = sfRenderWindow_mapCoordsToPixel(self._RenderWindow, point._Vector2f[0], NULL)
		return Vector2i(pixel.x, pixel.y)

	cpdef draw_sprite(self, Sprite sprite, RenderStates states=None):
		if states is None:
			sfRenderWindow_drawSprite(self._RenderWindow, sprite._Sprite, NULL)
		else:
			sfRenderWindow_drawSprite(self._RenderWindow, sprite._Sprite, states._RenderStates)

	cpdef draw_text(self, Text text, RenderStates states=None):
		if states is None:
			sfRenderWindow_drawText(self._RenderWindow, text._Text, NULL)
		else:
			sfRenderWindow_drawText(self._RenderWindow, text._Text, states._RenderStates)

	cpdef draw_rectangle_shape(self, RectangleShape rectangle_shape, RenderStates states=None):
		if states is None:
			sfRenderWindow_drawRectangleShape(self._RenderWindow, rectangle_shape._RectangleShape, NULL)
		else:
			sfRenderWindow_drawRectangleShape(self._RenderWindow, rectangle_shape._RectangleShape, states._RenderStates)

	cpdef draw(self, object drawable, RenderStates states=None):
		if isinstance(drawable, Sprite):
			self.draw_sprite(drawable, states)
		elif isinstance(drawable, Text):
			self.draw_text(drawable, states)
		elif isinstance(drawable, RectangleShape):
			self.draw_rectangle_shape(drawable, states)
