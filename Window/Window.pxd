#from Export cimport *
from .Event cimport *
from .VideoMode cimport *
from .WindowHandle cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Window/Window.h>":

	ctypedef enum sfWindowStyle:
		sfNone         = 0,
		sfTitlebar     = 1 << 0,
		sfResize       = 1 << 1,
		sfClose        = 1 << 2,
		sfFullscreen   = 1 << 3,
		sfDefaultStyle = sfTitlebar | sfResize | sfClose

	ctypedef enum sfContextAttribute:
		sfContextDefault = 0,
		sfContextCore    = 1 << 0,
		sfContextDebug   = 1 << 2

	ctypedef struct sfContextSettings:
		unsigned int depthBits
		unsigned int stencilBits
		unsigned int antialiasingLevel
		unsigned int majorVersion
		unsigned int minorVersion
		unsigned int attributeFlags
		bint         sRgbCapable

	sfWindow* sfWindow_create(sfVideoMode mode, const char* title, unsigned int style, sfContextSettings* settings)
	sfWindow* sfWindow_createUnicode(sfVideoMode mode, const unsigned int* title, unsigned int style, const sfContextSettings* settings)
	sfWindow* sfWindow_createFromHandle(sfWindowHandle handle, const sfContextSettings* settings)
	void sfWindow_destroy(sfWindow* window)
	void sfWindow_close(sfWindow* window)
	bint sfWindow_isOpen(const sfWindow* window)
	sfContextSettings sfWindow_getSettings(const sfWindow* window)
	bint sfWindow_pollEvent(sfWindow* window, sfEvent* event)
	bint sfWindow_waitEvent(sfWindow* window, sfEvent* event)
	sfVector2i sfWindow_getPosition(const sfWindow* window)
	void sfWindow_setPosition(sfWindow* window, sfVector2i position)
	sfVector2u sfWindow_getSize(const sfWindow* window)
	void sfWindow_setSize(sfWindow* window, sfVector2u size)
	void sfWindow_setTitle(sfWindow* window, const char* title)
	void sfWindow_setUnicodeTitle(sfWindow* window, const unsigned int* title)
	void sfWindow_setIcon(sfWindow* window, unsigned int width, unsigned int height, const unsigned char* pixels)
	void sfWindow_setVisible(sfWindow* window, bint visible)
	void sfWindow_setVerticalSyncEnabled(sfWindow* window, bint enabled)
	void sfWindow_setMouseCursorVisible(sfWindow* window, bint visible)
	void sfWindow_setMouseCursorGrabbed(sfWindow* window, bint grabbed)
	void sfWindow_setKeyRepeatEnabled(sfWindow* window, bint enabled)
	void sfWindow_setFramerateLimit(sfWindow* window, unsigned int limit)
	void sfWindow_setJoystickThreshold(sfWindow* window, float threshold)
	bint sfWindow_setActive(sfWindow* window, bint active)
	void sfWindow_requestFocus(sfWindow* window)
	bint sfWindow_hasFocus(const sfWindow* window)
	void sfWindow_display(sfWindow* window)
	sfWindowHandle sfWindow_getSystemHandle(const sfWindow* window)
