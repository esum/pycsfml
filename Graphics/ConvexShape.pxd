#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Transform cimport *
from .Types cimport *
from System.Vector2 cimport *
from stddef import size_t


cdef extern from "<SFML/Graphics/ConvexShape.h>":

	sfConvexShape* sfConvexShape_create()
	sfConvexShape* sfConvexShape_copy(const sfConvexShape* shape)
	void sfConvexShape_destroy(sfConvexShape* shape)
	void sfConvexShape_setPosition(sfConvexShape* shape, sfVector2f position)
	void sfConvexShape_setRotation(sfConvexShape* shape, float angle)
	void sfConvexShape_setScale(sfConvexShape* shape, sfVector2f scale)
	void sfConvexShape_setOrigin(sfConvexShape* shape, sfVector2f origin)
	sfVector2f sfConvexShape_getPosition(const sfConvexShape* shape)
	float sfConvexShape_getRotation(const sfConvexShape* shape)
	sfVector2f sfConvexShape_getScale(const sfConvexShape* shape)
	sfVector2f sfConvexShape_getOrigin(const sfConvexShape* shape)
	void sfConvexShape_move(sfConvexShape* shape, sfVector2f offset)
	void sfConvexShape_rotate(sfConvexShape* shape, float angle)
	void sfConvexShape_scale(sfConvexShape* shape, sfVector2f factors)
	sfTransform sfConvexShape_getTransform(const sfConvexShape* shape)
	sfTransform sfConvexShape_getInverseTransform(const sfConvexShape* shape)
	void sfConvexShape_setTexture(sfConvexShape* shape, const sfTexture* texture, bint resetRect)
	void sfConvexShape_setTextureRect(sfConvexShape* shape, sfIntRect rect)
	void sfConvexShape_setFillColor(sfConvexShape* shape, sfColor color)
	void sfConvexShape_setOutlineColor(sfConvexShape* shape, sfColor color)
	void sfConvexShape_setOutlineThickness(sfConvexShape* shape, float thickness)
	const sfTexture* sfConvexShape_getTexture(const sfConvexShape* shape)
	sfIntRect sfConvexShape_getTextureRect(const sfConvexShape* shape)
	sfColor sfConvexShape_getFillColor(const sfConvexShape* shape)
	sfColor sfConvexShape_getOutlineColor(const sfConvexShape* shape)
	float sfConvexShape_getOutlineThickness(const sfConvexShape* shape)
	size_t sfConvexShape_getPointCount(const sfConvexShape* shape)
	sfVector2f sfConvexShape_getPoint(const sfConvexShape* shape, size_t index)
	void sfConvexShape_setPointCount(sfConvexShape* shape, size_t count)
	void sfConvexShape_setPoint(sfConvexShape* shape, size_t index, sfVector2f point)
	sfFloatRect sfConvexShape_getLocalBounds(const sfConvexShape* shape)
	sfFloatRect sfConvexShape_getGlobalBounds(const sfConvexShape* shape)
