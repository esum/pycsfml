#from .Export cimport *
from .SoundStatus cimport *
from .Types cimport *
from System.Time cimport *
from System.Vector3 cimport *


cdef extern from "<SFML/Audio/Sound.h>":

	sfSound* sfSound_create()
	sfSound* sfSound_copy(const sfSound* sound)
	void sfSound_destroy(sfSound* sound)
	void sfSound_play(sfSound* sound)
	void sfSound_pause(sfSound* sound)
	void sfSound_stop(sfSound* sound)
	void sfSound_setBuffer(sfSound* sound, const sfSoundBuffer* buffer)
	const sfSoundBuffer* sfSound_getBuffer(const sfSound* sound)
	void sfSound_setLoop(sfSound* sound, bint loop)
	bint sfSound_getLoop(const sfSound* sound)
	sfSoundStatus sfSound_getStatus(const sfSound* sound)
	void sfSound_setPitch(sfSound* sound, float pitch)
	void sfSound_setVolume(sfSound* sound, float volume)
	void sfSound_setPosition(sfSound* sound, sfVector3f position)
	void sfSound_setRelativeToListener(sfSound* sound, bint relative)
	void sfSound_setMinDistance(sfSound* sound, float distance)
	void sfSound_setAttenuation(sfSound* sound, float attenuation)
	void sfSound_setPlayingOffset(sfSound* sound, sfTime timeOffset)
	float sfSound_getPitch(const sfSound* sound)
	float sfSound_getVolume(const sfSound* sound)
	sfVector3f sfSound_getPosition(const sfSound* sound)
	bint sfSound_isRelativeToListener(const sfSound* sound)
	float sfSound_getMinDistance(const sfSound* sound)
	float sfSound_getAttenuation(const sfSound* sound)
	sfTime sfSound_getPlayingOffset(const sfSound* sound)
