#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Transform cimport *
from .Types cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/RectangleShape.h>":

	sfRectangleShape* sfRectangleShape_create()
	sfRectangleShape* sfRectangleShape_copy(const sfRectangleShape* shape)
	void sfRectangleShape_destroy(sfRectangleShape* shape)
	void sfRectangleShape_setPosition(sfRectangleShape* shape, sfVector2f position)
	void sfRectangleShape_setRotation(sfRectangleShape* shape, float angle)
	void sfRectangleShape_setScale(sfRectangleShape* shape, sfVector2f scale)
	void sfRectangleShape_setOrigin(sfRectangleShape* shape, sfVector2f origin)
	sfVector2f sfRectangleShape_getPosition(const sfRectangleShape* shape)
	float sfRectangleShape_getRotation(const sfRectangleShape* shape)
	sfVector2f sfRectangleShape_getScale(const sfRectangleShape* shape)
	sfVector2f sfRectangleShape_getOrigin(const sfRectangleShape* shape)
	void sfRectangleShape_move(sfRectangleShape* shape, sfVector2f offset)
	void sfRectangleShape_rotate(sfRectangleShape* shape, float angle)
	void sfRectangleShape_scale(sfRectangleShape* shape, sfVector2f factors)
	sfTransform sfRectangleShape_getTransform(const sfRectangleShape* shape)
	sfTransform sfRectangleShape_getInverseTransform(const sfRectangleShape* shape)
	void sfRectangleShape_setTexture(sfRectangleShape* shape, const sfTexture* texture, bint resetRect)
	void sfRectangleShape_setTextureRect(sfRectangleShape* shape, sfIntRect rect)
	void sfRectangleShape_setFillColor(sfRectangleShape* shape, sfColor color)
	void sfRectangleShape_setOutlineColor(sfRectangleShape* shape, sfColor color)
	void sfRectangleShape_setOutlineThickness(sfRectangleShape* shape, float thickness)
	const sfTexture* sfRectangleShape_getTexture(const sfRectangleShape* shape)
	sfIntRect sfRectangleShape_getTextureRect(const sfRectangleShape* shape)
	sfColor sfRectangleShape_getFillColor(const sfRectangleShape* shape)
	sfColor sfRectangleShape_getOutlineColor(const sfRectangleShape* shape)
	float sfRectangleShape_getOutlineThickness(const sfRectangleShape* shape)
	size_t sfRectangleShape_getPointCount(const sfRectangleShape* shape)
	sfVector2f sfRectangleShape_getPoint(const sfRectangleShape* shape, size_t index)
	void sfRectangleShape_setSize(sfRectangleShape* shape, sfVector2f size)
	sfVector2f sfRectangleShape_getSize(const sfRectangleShape* shape)
	sfFloatRect sfRectangleShape_getLocalBounds(const sfRectangleShape* shape)
	sfFloatRect sfRectangleShape_getGlobalBounds(const sfRectangleShape* shape)
