#from Export cimport *
from .Types cimport *
from .Window cimport *


cdef extern from "<SFML/Window/Context.h>":

	sfContext* sfContext_create()
	void sfContext_destroy(sfContext* context)
	bint sfContext_setActive(sfContext* context, bint active)
	sfContextSettings sfContext_getSettings(const sfContext* context)
