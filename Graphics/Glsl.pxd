from .Transform cimport *
from .Color cimport *
from System.Vector2 cimport *
from System.Vector3 cimport *


cdef extern from "<SFML/Graphics/Glsl.h>":

	ctypedef sfVector2f sfGlslVec2
	ctypedef sfVector2i sfGlslIvec2

	ctypedef struct sfGlslBvec2:
		bint x
		bint y

	ctypedef sfVector3f sfGlslVec3

	ctypedef struct sfGlslIvec3:
		int x
		int y
		int z

	ctypedef struct sfGlslBvec3:
		bint x
		bint y
		bint z

	ctypedef struct sfGlslVec4:
		float x
		float y
		float z
		float w

	ctypedef struct sfGlslIvec4:
		int x
		int y
		int z
		int w

	ctypedef struct sfGlslBvec4:
		bint x
		bint y
		bint z
		bint w

	ctypedef struct sfGlslMat3:
		float array[3 * 3]

	ctypedef struct sfGlslMat4:
		float array[4 * 4]
