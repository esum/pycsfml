#from Export cimport *
from .BlendMode cimport *
from .Transform cimport *
from .Types cimport *


cdef extern from "<SFML/Graphics/RenderStates.h>":

	ctypedef struct sfRenderStates:
		sfBlendMode      blendMode
		sfTransform      transform
		const sfTexture* texture
		const sfShader*  shader
