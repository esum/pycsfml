#from .Export cimport *
from .Types cimport *


cdef extern from "<SFML/System/Mutex.h>":

	sfMutex* sfMutex_create()
	void sfMutex_destroy(sfMutex* mutex)
	void sfMutex_lock(sfMutex* mutex)
	void sfMutex_unlock(sfMutex* mutex)
