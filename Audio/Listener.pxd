#from Audio.Export cimport *
from System.Vector3 cimport *


cdef extern from "<SFML/Audio/Listener.h>":

	void sfListener_setGlobalVolume(float volume)
	float sfListener_getGlobalVolume()
	void sfListener_setPosition(sfVector3f position)
	sfVector3f sfListener_getPosition()
	void sfListener_setDirection(sfVector3f direction)
	sfVector3f sfListener_getDirection()
	void sfListener_setUpVector(sfVector3f upVector)
	sfVector3f sfListener_getUpVector()
