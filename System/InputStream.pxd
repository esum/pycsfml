#from .Export cimport *


cdef extern from "<SFML/System/InputStream.h>":

	ctypedef long long (*sfInputStreamReadFunc)(void* data, long long size, void* userData)
	ctypedef long long (*sfInputStreamSeekFunc)(long long position, void* userData)
	ctypedef long long (*sfInputStreamTellFunc)(void* userData)
	ctypedef long long (*sfInputStreamGetSizeFunc)(void* userData)

	ctypedef struct sfInputStream:
		sfInputStreamReadFunc    read
		sfInputStreamSeekFunc    seek
		sfInputStreamTellFunc    tell
		sfInputStreamGetSizeFunc getSize
		void*                    userData
