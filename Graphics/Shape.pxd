#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Transform cimport *
from .Types cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Shape.h>":

	ctypedef size_t (*sfShapeGetPointCountCallback)(void*)
	ctypedef sfVector2f (*sfShapeGetPointCallback)(size_t, void*)
	sfShape* sfShape_create(sfShapeGetPointCountCallback getPointCount, sfShapeGetPointCallback getPoint, void* userData)
	void sfShape_destroy(sfShape* shape)
	void sfShape_setPosition(sfShape* shape, sfVector2f position)
	void sfShape_setRotation(sfShape* shape, float angle)
	void sfShape_setScale(sfShape* shape, sfVector2f scale)
	void sfShape_setOrigin(sfShape* shape, sfVector2f origin)
	sfVector2f sfShape_getPosition(const sfShape* shape)
	float sfShape_getRotation(const sfShape* shape)
	sfVector2f sfShape_getScale(const sfShape* shape)
	sfVector2f sfShape_getOrigin(const sfShape* shape)
	void sfShape_move(sfShape* shape, sfVector2f offset)
	void sfShape_rotate(sfShape* shape, float angle)
	void sfShape_scale(sfShape* shape, sfVector2f factors)
	sfTransform sfShape_getTransform(const sfShape* shape)
	sfTransform sfShape_getInverseTransform(const sfShape* shape)
	void sfShape_setTexture(sfShape* shape, const sfTexture* texture, bint resetRect)
	void sfShape_setTextureRect(sfShape* shape, sfIntRect rect)
	void sfShape_setFillColor(sfShape* shape, sfColor color)
	void sfShape_setOutlineColor(sfShape* shape, sfColor color)
	void sfShape_setOutlineThickness(sfShape* shape, float thickness)
	const sfTexture* sfShape_getTexture(const sfShape* shape)
	sfIntRect sfShape_getTextureRect(const sfShape* shape)
	sfColor sfShape_getFillColor(const sfShape* shape)
	sfColor sfShape_getOutlineColor(const sfShape* shape)
	float sfShape_getOutlineThickness(const sfShape* shape)
	size_t sfShape_getPointCount(const sfShape* shape)
	sfVector2f sfShape_getPoint(const sfShape* shape, size_t index)
	sfFloatRect sfShape_getLocalBounds(const sfShape* shape)
	sfFloatRect sfShape_getGlobalBounds(const sfShape* shape)
	void sfShape_update(sfShape* shape)
