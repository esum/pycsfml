from .Rect cimport *


cdef extern from "<SFML/Graphics/Glyph.h>":

	ctypedef struct sfGlyph:
		float       advance
		sfFloatRect bounds
		sfIntRect   textureRect
