#from .Export cimport *
from .Types cimport *
from System.InputStream cimport *
from System.Time cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Audio/SoundBuffer.h>":

	sfSoundBuffer* sfSoundBuffer_createFromFile(const char* filename)
	sfSoundBuffer* sfSoundBuffer_createFromMemory(const void* data, size_t sizeInBytes)
	sfSoundBuffer* sfSoundBuffer_createFromStream(sfInputStream* stream)
	sfSoundBuffer* sfSoundBuffer_createFromSamples(const short* samples, sfUint64 sampleCount, unsigned int channelCount, unsigned int sampleRate)
	sfSoundBuffer* sfSoundBuffer_copy(const sfSoundBuffer* soundBuffer)
	void sfSoundBuffer_destroy(sfSoundBuffer* soundBuffer)
	bint sfSoundBuffer_saveToFile(const sfSoundBuffer* soundBuffer, const char* filename)
	const short* sfSoundBuffer_getSamples(const sfSoundBuffer* soundBuffer)
	unsigned long long sfSoundBuffer_getSampleCount(const sfSoundBuffer* soundBuffer)
	unsigned int sfSoundBuffer_getSampleRate(const sfSoundBuffer* soundBuffer)
	unsigned int sfSoundBuffer_getChannelCount(const sfSoundBuffer* soundBuffer)
	sfTime sfSoundBuffer_getDuration(const sfSoundBuffer* soundBuffer)
