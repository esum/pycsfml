#from .Export cimport *
from .Types cimport *


cdef extern from "<SFML/System/Thread.h>":

	sfThread* sfThread_create(void (*function)(void*), void* userData)
	void sfThread_destroy(sfThread* thread)
	void sfThread_launch(sfThread* thread)
	void sfThread_wait(sfThread* thread)
	void sfThread_terminate(sfThread* thread)
