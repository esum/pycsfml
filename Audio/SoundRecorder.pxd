#from .Export cimport *
from .Types cimport *
from System.Time cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Audio/SoundRecorder>"

	ctypedef bint (*sfSoundRecorderStartCallback)(void*)
	ctypedef bint (*sfSoundRecorderProcessCallback)(const short*, size_t, void*)
	ctypedef void (*sfSoundRecorderStopCallback)(void*)

	sfSoundRecorder* sfSoundRecorder_create(sfSoundRecorderStartCallback onStart, sfSoundRecorderProcessCallback onProcess, sfSoundRecorderStopCallback onStop, void* userData)
	void sfSoundRecorder_destroy(sfSoundRecorder* soundRecorder)
	bint sfSoundRecorder_start(sfSoundRecorder* soundRecorder, unsigned int sampleRate)
	void sfSoundRecorder_stop(sfSoundRecorder* soundRecorder)
	unsigned int sfSoundRecorder_getSampleRate(const sfSoundRecorder* soundRecorder)
	bint sfSoundRecorder_isAvailable()
	void sfSoundRecorder_setProcessingInterval(sfSoundRecorder* soundRecorder, sfTime interval)
	const char** sfSoundRecorder_getAvailableDevices(size_t* count)
	const char* sfSoundRecorder_getDefaultDevice()
	bint sfSoundRecorder_setDevice(sfSoundRecorder* soundRecorder, const char* name)
	const char* sfSoundRecorder_getDevice(sfSoundRecorder* soundRecorder)
	void sfSoundRecorder_setChannelCount(sfSoundRecorder* soundRecorder, unsigned int channelCount)
	unsigned int sfSoundRecorder_getChannelCount(const sfSoundRecorder* soundRecorder)
