#from .Export cimport *


cdef extern from "<SFML/Graphics/BlendMode.h>":

	ctypedef enum sfBlendFactor:
		sfBlendFactorZero,
		sfBlendFactorOne,
		sfBlendFactorSrcColor,
		sfBlendFactorOneMinusSrcColor,
		sfBlendFactorDstColor,
		sfBlendFactorOneMinusDstColor,
		sfBlendFactorSrcAlpha,
		sfBlendFactorOneMinusSrcAlpha,
		sfBlendFactorDstAlpha,
		sfBlendFactorOneMinusDstAlpha

	ctypedef enum sfBlendEquation:
		sfBlendEquationAdd,
		sfBlendEquationSubtract,
		sfBlendEquationReverseSubtract

	ctypedef struct sfBlendMode:
		sfBlendFactor colorSrcFactor
		sfBlendFactor colorDstFactor
		sfBlendEquation colorEquation
		sfBlendFactor alphaSrcFactor
		sfBlendFactor alphaDstFactor
		sfBlendEquation alphaEquation

	const sfBlendMode sfBlendAlpha
	const sfBlendMode sfBlendAdd
	const sfBlendMode sfBlendMultiply
	const sfBlendMode sfBlendNone
