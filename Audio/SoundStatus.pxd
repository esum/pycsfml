#from .Export cimport *


cdef extern from "<SFML/Audio/SoundStatus.h>":

	ctypedef enum sfSoundStatus:
		sfStopped,
		sfPaused,
		sfPlaying
