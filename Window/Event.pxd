#from Export cimport *
from .Joystick cimport *
from .Keyboard cimport *
from .Mouse cimport *
from .Sensor cimport *


cdef extern from "<SFML/Window/Event.h>":

	ctypedef enum sfEventType:
		sfEvtClosed,
		sfEvtResized,
		sfEvtLostFocus,
		sfEvtGainedFocus,
		sfEvtTextEntered,
		sfEvtKeyPressed,
		sfEvtKeyReleased,
		sfEvtMouseWheelMoved,
		sfEvtMouseWheelScrolled,
		sfEvtMouseButtonPressed,
		sfEvtMouseButtonReleased,
		sfEvtMouseMoved,
		sfEvtMouseEntered,
		sfEvtMouseLeft,
		sfEvtJoystickButtonPressed,
		sfEvtJoystickButtonReleased,
		sfEvtJoystickMoved,
		sfEvtJoystickConnected,
		sfEvtJoystickDisconnected,
		sfEvtTouchBegan,
		sfEvtTouchMoved,
		sfEvtTouchEnded,
		sfEvtSensorChanged,
		sfEvtCount,

	ctypedef struct sfKeyEvent:
		sfEventType type
		sfKeyCode   code
		bint      alt
		bint      control
		bint      shift
		bint      system

	ctypedef struct sfTextEvent:
		sfEventType type
		unsigned int    unicode

	ctypedef struct sfMouseMoveEvent:
		sfEventType type
		int         x
		int         y

	ctypedef struct sfMouseButtonEvent:
		sfEventType   type
		sfMouseButton button
		int           x
		int           y

	ctypedef struct sfMouseWheelEvent:
		sfEventType type
		int         delta
		int         x
		int         y

	ctypedef struct sfMouseWheelScrollEvent:
		sfEventType  type
		sfMouseWheel wheel
		float        delta
		int          x
		int          y

	ctypedef struct sfJoystickMoveEvent:
		sfEventType    type
		unsigned int   joystickId
		sfJoystickAxis axis
		float          position

	ctypedef struct sfJoystickButtonEvent:
		sfEventType  type
		unsigned int joystickId
		unsigned int button

	ctypedef struct sfJoystickConnectEvent:
		sfEventType  type
		unsigned int joystickId

	ctypedef struct sfSizeEvent:
		sfEventType  type
		unsigned int width
		unsigned int height

	ctypedef struct sfTouchEvent:
		sfEventType  type
		unsigned int finger
		int          x
		int          y

	ctypedef struct sfSensorEvent:
		sfEventType  type
		sfSensorType sensorType
		float        x
		float        y
		float        z

	ctypedef union sfEvent:
		sfEventType             type
		sfSizeEvent             size
		sfKeyEvent              key
		sfTextEvent             text
		sfMouseMoveEvent        mouseMove
		sfMouseButtonEvent      mouseButton
		sfMouseWheelEvent       mouseWheel
		sfMouseWheelScrollEvent mouseWheelScroll
		sfJoystickMoveEvent     joystickMove
		sfJoystickButtonEvent   joystickButton
		sfJoystickConnectEvent  joystickConnect
		sfTouchEvent            touch
		sfSensorEvent           sensor
