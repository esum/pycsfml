#from .Export cimport *


cdef extern from "<SFML/System/Vector2.h>":

	ctypedef struct sfVector2i:
		int x
		int y

	ctypedef struct sfVector2u:
		unsigned int x
		unsigned int y

	ctypedef struct sfVector2f:
		float x
		float y
