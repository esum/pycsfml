#from Export cimport *
from .Rect cimport *
from .Types cimport *
from Window.Types cimport *
from System.InputStream cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Texture.h>":

	sfTexture* sfTexture_create(unsigned int width, unsigned int height)
	sfTexture* sfTexture_createFromFile(const char* filename, const sfIntRect* area)
	sfTexture* sfTexture_createFromMemory(const void* data, size_t sizeInBytes, const sfIntRect* area)
	sfTexture* sfTexture_createFromStream(sfInputStream* stream, const sfIntRect* area)
	sfTexture* sfTexture_createFromImage(const sfImage* image, const sfIntRect* area)
	sfTexture* sfTexture_copy(const sfTexture* texture)
	void sfTexture_destroy(sfTexture* texture)
	sfVector2u sfTexture_getSize(const sfTexture* texture)
	sfImage* sfTexture_copyToImage(const sfTexture* texture)
	void sfTexture_updateFromPixels(sfTexture* texture, const unsigned char* pixels, unsigned int width, unsigned int height, unsigned int x, unsigned int y)
	void sfTexture_updateFromImage(sfTexture* texture, const sfImage* image, unsigned int x, unsigned int y)
	void sfTexture_updateFromWindow(sfTexture* texture, const sfWindow* window, unsigned int x, unsigned int y)
	void sfTexture_updateFromRenderWindow(sfTexture* texture, const sfRenderWindow* renderWindow, unsigned int x, unsigned int y)
	void sfTexture_setSmooth(sfTexture* texture, bint smooth)
	bint sfTexture_isSmooth(const sfTexture* texture)
	void sfTexture_setSrgb(sfTexture* texture, bint sRgb)
	bint sfTexture_isSrgb(const sfTexture* texture)
	void sfTexture_setRepeated(sfTexture* texture, bint repeated)
	bint sfTexture_isRepeated(const sfTexture* texture)
	bint sfTexture_generateMipmap(sfTexture* texture)
	unsigned int sfTexture_getNativeHandle(const sfTexture* texture)
	void sfTexture_bind(const sfTexture* texture)
	unsigned int sfTexture_getMaximumSize()
