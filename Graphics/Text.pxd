#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Types cimport *
from .Transform cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Text.h>":

	ctypedef enum sfTextStyle:
		sfTextRegular       = 0
		sfTextBold          = 1 << 0
		sfTextItalic        = 1 << 1
		sfTextUnderlined    = 1 << 2
		sfTextStrikeThrough = 1 << 3

	sfText* sfText_create()
	sfText* sfText_copy(const sfText* text)
	void sfText_destroy(sfText* text)
	void sfText_setPosition(sfText* text, sfVector2f position)
	void sfText_setRotation(sfText* text, float angle)
	void sfText_setScale(sfText* text, sfVector2f scale)
	void sfText_setOrigin(sfText* text, sfVector2f origin)
	sfVector2f sfText_getPosition(const sfText* text)
	float sfText_getRotation(const sfText* text)
	sfVector2f sfText_getScale(const sfText* text)
	sfVector2f sfText_getOrigin(const sfText* text)
	void sfText_move(sfText* text, sfVector2f offset)
	void sfText_rotate(sfText* text, float angle)
	void sfText_scale(sfText* text, sfVector2f factors)
	sfTransform sfText_getTransform(const sfText* text)
	sfTransform sfText_getInverseTransform(const sfText* text)
	void sfText_setString(sfText* text, const char* string)
	void sfText_setUnicodeString(sfText* text, const unsigned int* string)
	void sfText_setFont(sfText* text, const sfFont* font)
	void sfText_setCharacterSize(sfText* text, unsigned int size)
	void sfText_setStyle(sfText* text, unsigned int style)
	void sfText_setColor(sfText* text, sfColor color)
	void sfText_setFillColor(sfText* text, sfColor color)
	void sfText_setOutlineColor(sfText* text, sfColor color)
	void sfText_setOutlineThickness(sfText* text, float thickness)
	const char* sfText_getString(const sfText* text)
	const unsigned int* sfText_getUnicodeString(const sfText* text)
	const sfFont* sfText_getFont(const sfText* text)
	unsigned int sfText_getCharacterSize(const sfText* text)
	unsigned int sfText_getStyle(const sfText* text)
	sfColor sfText_getColor(const sfText* text)
	sfColor sfText_getFillColor(const sfText* text)
	sfColor sfText_getOutlineColor(const sfText* text)
	float sfText_getOutlineThickness(const sfText* text)
	sfVector2f sfText_findCharacterPos(const sfText* text, size_t index)
	sfFloatRect sfText_getLocalBounds(const sfText* text)
	sfFloatRect sfText_getGlobalBounds(const sfText* text)
