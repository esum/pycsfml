#from Export cimport *
from .BlendMode cimport *
from .Color cimport *
from .Rect cimport *
from .Transform cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Graphics/Sprite.h>":

	sfSprite* sfSprite_create()
	sfSprite* sfSprite_copy(const sfSprite* sprite)
	void sfSprite_destroy(sfSprite* sprite)
	void sfSprite_setPosition(sfSprite* sprite, sfVector2f position)
	void sfSprite_setRotation(sfSprite* sprite, float angle)
	void sfSprite_setScale(sfSprite* sprite, sfVector2f scale)
	void sfSprite_setOrigin(sfSprite* sprite, sfVector2f origin)
	sfVector2f sfSprite_getPosition(const sfSprite* sprite)
	float sfSprite_getRotation(const sfSprite* sprite)
	sfVector2f sfSprite_getScale(const sfSprite* sprite)
	sfVector2f sfSprite_getOrigin(const sfSprite* sprite)
	void sfSprite_move(sfSprite* sprite, sfVector2f offset)
	void sfSprite_rotate(sfSprite* sprite, float angle)
	void sfSprite_scale(sfSprite* sprite, sfVector2f factors)
	sfTransform sfSprite_getTransform(const sfSprite* sprite)
	sfTransform sfSprite_getInverseTransform(const sfSprite* sprite)
	void sfSprite_setTexture(sfSprite* sprite, const sfTexture* texture, bint resetRect)
	void sfSprite_setTextureRect(sfSprite* sprite, sfIntRect rectangle)
	void sfSprite_setColor(sfSprite* sprite, sfColor color)
	const sfTexture* sfSprite_getTexture(const sfSprite* sprite)
	sfIntRect sfSprite_getTextureRect(const sfSprite* sprite)
	sfColor sfSprite_getColor(const sfSprite* sprite)
	sfFloatRect sfSprite_getLocalBounds(const sfSprite* sprite)
	sfFloatRect sfSprite_getGlobalBounds(const sfSprite* sprite)
