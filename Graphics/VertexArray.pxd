#from Export cimport *
from .PrimitiveType cimport *
from .Rect cimport *
from .Types cimport *
from .Vertex cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/VertexArray.h>":

	sfVertexArray* sfVertexArray_create()
	sfVertexArray* sfVertexArray_copy(const sfVertexArray* vertexArray)
	void sfVertexArray_destroy(sfVertexArray* vertexArray)
	size_t sfVertexArray_getVertexCount(const sfVertexArray* vertexArray)
	sfVertex* sfVertexArray_getVertex(sfVertexArray* vertexArray, size_t index)
	void sfVertexArray_clear(sfVertexArray* vertexArray)
	void sfVertexArray_resize(sfVertexArray* vertexArray, size_t vertexCount)
	void sfVertexArray_append(sfVertexArray* vertexArray, sfVertex vertex)
	void sfVertexArray_setPrimitiveType(sfVertexArray* vertexArray, sfPrimitiveType type)
	sfPrimitiveType sfVertexArray_getPrimitiveType(sfVertexArray* vertexArray)
	sfFloatRect sfVertexArray_getBounds(sfVertexArray* vertexArray)
