#from Export cimport *
from .Rect cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Graphics/View.h>":

	sfView* sfView_create()
	sfView* sfView_createFromRect(sfFloatRect rectangle)
	sfView* sfView_copy(const sfView* view)
	void sfView_destroy(sfView* view)
	void sfView_setCenter(sfView* view, sfVector2f center)
	void sfView_setSize(sfView* view, sfVector2f size)
	void sfView_setRotation(sfView* view, float angle)
	void sfView_setViewport(sfView* view, sfFloatRect viewport)
	void sfView_reset(sfView* view, sfFloatRect rectangle)
	sfVector2f sfView_getCenter(const sfView* view)
	sfVector2f sfView_getSize(const sfView* view)
	float sfView_getRotation(const sfView* view)
	sfFloatRect sfView_getViewport(const sfView* view)
	void sfView_move(sfView* view, sfVector2f offset)
	void sfView_rotate(sfView* view, float angle)
	void sfView_zoom(sfView* view, float factor)
