#from .Export cimport *
from .Time cimport *
from .Types cimport *


cdef extern from "<SFML/System/Clock.h>":

	sfClock* sfClock_create()
	sfClock* sfClock_copy(const sfClock* clock)
	void sfClock_destroy(sfClock* clock)
	sfTime sfClock_getElapsedTime(const sfClock* clock)
	sfTime sfClock_restart(sfClock* clock)
