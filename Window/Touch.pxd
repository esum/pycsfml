#from .Export cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Window/Touch.h>":

	bint sfTouch_isDown(unsigned int finger)
	sfVector2i sfTouch_getPosition(unsigned int finger, const sfWindow* relativeTo)
