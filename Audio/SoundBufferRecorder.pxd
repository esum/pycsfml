#from .Export cimport *
from .Types cimport *


cdef extern from "<SFML/Audio/SoundBufferRecorder.h>":

	sfSoundBufferRecorder* sfSoundBufferRecorder_create()
	void sfSoundBufferRecorder_destroy(sfSoundBufferRecorder* soundBufferRecorder)
	bint sfSoundBufferRecorder_start(sfSoundBufferRecorder* soundBufferRecorder, unsigned int sampleRate)
	void sfSoundBufferRecorder_stop(sfSoundBufferRecorder* soundBufferRecorder)
	unsigned int sfSoundBufferRecorder_getSampleRate(const sfSoundBufferRecorder* soundBufferRecorder)
	const sfSoundBuffer* sfSoundBufferRecorder_getBuffer(const sfSoundBufferRecorder* soundBufferRecorder)
	bint sfSoundBufferRecorder_setDevice(sfSoundBufferRecorder* soundBufferRecorder, const char* name)
	const char* sfSoundBufferRecorder_getDevice(sfSoundBufferRecorder* soundBufferRecorder)
