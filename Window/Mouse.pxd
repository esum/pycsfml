#from Export cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Window/Mouse.h>":

	ctypedef enum sfMouseButton:
		sfMouseLeft,
		sfMouseRight,
		sfMouseMiddle,
		sfMouseXButton1,
		sfMouseXButton2,
		sfMouseButtonCount

	ctypedef enum sfMouseWheel:
		sfMouseVerticalWheel,
		sfMouseHorizontalWheel

	bint sfMouse_isButtonPressed(sfMouseButton button)
	sfVector2i sfMouse_getPosition(const sfWindow* relativeTo)
	void sfMouse_setPosition(sfVector2i position, const sfWindow* relativeTo)
