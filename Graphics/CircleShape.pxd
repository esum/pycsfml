#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Transform cimport *
from .Types cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/CircleShape.h>":

	sfCircleShape* sfCircleShape_create()
	sfCircleShape* sfCircleShape_copy(const sfCircleShape* shape)
	void sfCircleShape_destroy(sfCircleShape* shape)
	void sfCircleShape_setPosition(sfCircleShape* shape, sfVector2f position)
	void sfCircleShape_setRotation(sfCircleShape* shape, float angle)
	void sfCircleShape_setScale(sfCircleShape* shape, sfVector2f scale)
	void sfCircleShape_setOrigin(sfCircleShape* shape, sfVector2f origin)
	sfVector2f sfCircleShape_getPosition(const sfCircleShape* shape)
	float sfCircleShape_getRotation(const sfCircleShape* shape)
	sfVector2f sfCircleShape_getScale(const sfCircleShape* shape)
	sfVector2f sfCircleShape_getOrigin(const sfCircleShape* shape)
	void sfCircleShape_move(sfCircleShape* shape, sfVector2f offset)
	void sfCircleShape_rotate(sfCircleShape* shape, float angle)
	void sfCircleShape_scale(sfCircleShape* shape, sfVector2f factors)
	sfTransform sfCircleShape_getTransform(const sfCircleShape* shape)
	sfTransform sfCircleShape_getInverseTransform(const sfCircleShape* shape)
	void sfCircleShape_setTexture(sfCircleShape* shape, const sfTexture* texture, bint resetRect)
	void sfCircleShape_setTextureRect(sfCircleShape* shape, sfIntRect rect)
	void sfCircleShape_setFillColor(sfCircleShape* shape, sfColor color)
	void sfCircleShape_setOutlineColor(sfCircleShape* shape, sfColor color)
	void sfCircleShape_setOutlineThickness(sfCircleShape* shape, float thickness)
	const sfTexture* sfCircleShape_getTexture(const sfCircleShape* shape)
	sfIntRect sfCircleShape_getTextureRect(const sfCircleShape* shape)
	sfColor sfCircleShape_getFillColor(const sfCircleShape* shape)
	sfColor sfCircleShape_getOutlineColor(const sfCircleShape* shape)
	float sfCircleShape_getOutlineThickness(const sfCircleShape* shape)
	size_t sfCircleShape_getPointCount(const sfCircleShape* shape)
	sfVector2f sfCircleShape_getPoint(const sfCircleShape* shape, size_t index)
	void sfCircleShape_setRadius(sfCircleShape* shape, float radius)
	float sfCircleShape_getRadius(const sfCircleShape* shape)
	void sfCircleShape_setPointCount(sfCircleShape* shape, size_t count)
	sfFloatRect sfCircleShape_getLocalBounds(const sfCircleShape* shape)
	sfFloatRect sfCircleShape_getGlobalBounds(const sfCircleShape* shape)
