#from Export cimport *


cdef extern from "<SFML/Graphics/PrimitiveType.h>":

	ctypedef enum sfPrimitiveType:
		sfPoints,
		sfLines,
		sfLineStrip,
		sfTriangles,
		sfTriangleStrip,
		sfTriangleFan,
		sfQuads,
		sfLinesStrip     = sfLineStrip,
		sfTrianglesStrip = sfTriangleStrip,
		sfTrianglesFan   = sfTriangleFan
