#from .Export cimport *
from .SoundStatus cimport *
from .Types cimport *
from System.Time cimport *
from System.Vector3 cimport *


cdef extern from "<SFML/Audio/SoundStream.h>":

	ctypedef struct sfSoundStreamChunk:
		short*       samples
		unsigned int sampleCount

	ctypedef bint (*sfSoundStreamGetDataCallback)(sfSoundStreamChunk*, void*)
	ctypedef void (*sfSoundStreamSeekCallback)(sfTime, void*)

	sfSoundStream* sfSoundStream_create(sfSoundStreamGetDataCallback onGetData, sfSoundStreamSeekCallback onSeek, unsigned int channelCount, unsigned int sampleRate, void* userData)
	void sfSoundStream_destroy(sfSoundStream* soundStream)
	void sfSoundStream_play(sfSoundStream* soundStream)
	void sfSoundStream_pause(sfSoundStream* soundStream)
	void sfSoundStream_stop(sfSoundStream* soundStream)
	sfSoundStatus sfSoundStream_getStatus(const sfSoundStream* soundStream)
	unsigned int sfSoundStream_getChannelCount(const sfSoundStream* soundStream)
	unsigned int sfSoundStream_getSampleRate(const sfSoundStream* soundStream)
	void sfSoundStream_setPitch(sfSoundStream* soundStream, float pitch)
	void sfSoundStream_setVolume(sfSoundStream* soundStream, float volume)
	void sfSoundStream_setPosition(sfSoundStream* soundStream, sfVector3f position)
	void sfSoundStream_setRelativeToListener(sfSoundStream* soundStream, bint relative)
	void sfSoundStream_setMinDistance(sfSoundStream* soundStream, float distance)
	void sfSoundStream_setAttenuation(sfSoundStream* soundStream, float attenuation)
	void sfSoundStream_setPlayingOffset(sfSoundStream* soundStream, sfTime timeOffset)
	void sfSoundStream_setLoop(sfSoundStream* soundStream, bint loop)
	float sfSoundStream_getPitch(const sfSoundStream* soundStream)
	float sfSoundStream_getVolume(const sfSoundStream* soundStream)
	sfVector3f sfSoundStream_getPosition(const sfSoundStream* soundStream)
	bint sfSoundStream_isRelativeToListener(const sfSoundStream* soundStream)
	float sfSoundStream_getMinDistance(const sfSoundStream* soundStream)
	float sfSoundStream_getAttenuation(const sfSoundStream* soundStream)
	bint sfSoundStream_getLoop(const sfSoundStream* soundStream)
	sfTime sfSoundStream_getPlayingOffset(const sfSoundStream* soundStream)
