#from Export cimport *
from .Color cimport *
from .Glsl cimport *
from .Transform cimport *
from .Types cimport *
from System.InputStream cimport *
from System.Vector2 cimport *
from System.Vector3 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Shader.h>":

	sfShader* sfShader_createFromFile(const char* vertexShaderFilename, const char* geometryShaderFilename, const char* fragmentShaderFilename)
	sfShader* sfShader_createFromMemory(const char* vertexShader, const char* geometryShader, const char* fragmentShader)
	sfShader* sfShader_createFromStream(sfInputStream* vertexShaderStream, sfInputStream* geometryShaderStream, sfInputStream* fragmentShaderStream)
	void sfShader_destroy(sfShader* shader)
	void sfShader_setFloatUniform(sfShader* shader, const char* name, float x)
	void sfShader_setVec2Uniform(sfShader* shader, const char* name, sfGlslVec2 vector)
	void sfShader_setVec3Uniform(sfShader* shader, const char* name, sfGlslVec3 vector)
	void sfShader_setVec4Uniform(sfShader* shader, const char* name, sfGlslVec4 vector)
	void sfShader_setColorUniform(sfShader* shader, const char* name, sfColor color)
	void sfShader_setIntUniform(sfShader* shader, const char* name, int x)
	void sfShader_setIvec2Uniform(sfShader* shader, const char* name, sfGlslIvec2 vector)
	void sfShader_setIvec3Uniform(sfShader* shader, const char* name, sfGlslIvec3 vector)
	void sfShader_setIvec4Uniform(sfShader* shader, const char* name, sfGlslIvec4 vector)
	void sfShader_setIntColorUniform(sfShader* shader, const char* name, sfColor color)
	void sfShader_setBoolUniform(sfShader* shader, const char* name, bint x)
	void sfShader_setBvec2Uniform(sfShader* shader, const char* name, sfGlslBvec2 vector)
	void sfShader_setBvec3Uniform(sfShader* shader, const char* name, sfGlslBvec3 vector)
	void sfShader_setBvec4Uniform(sfShader* shader, const char* name, sfGlslBvec4 vector)
	void sfShader_setMat3Uniform(sfShader* shader, const char* name, const sfGlslMat3* matrix)
	void sfShader_setMat4Uniform(sfShader* shader, const char* name, const sfGlslMat4* matrix)
	void sfShader_setTextureUniform(sfShader* shader, const char* name, const sfTexture* texture)
	void sfShader_setCurrentTextureUniform(sfShader* shader, const char* name)
	void sfShader_setFloatUniformArray(sfShader* shader, const char* name, const float* scalarArray, size_t length)
	void sfShader_setVec2UniformArray(sfShader* shader, const char* name, const sfGlslVec2* vectorArray, size_t length)
	void sfShader_setVec3UniformArray(sfShader* shader, const char* name, const sfGlslVec3* vectorArray, size_t length)
	void sfShader_setVec4UniformArray(sfShader* shader, const char* name, const sfGlslVec4* vectorArray, size_t length)
	void sfShader_setMat3UniformArray(sfShader* shader, const char* name, const sfGlslMat3* matrixArray, size_t length)
	void sfShader_setMat4UniformArray(sfShader* shader, const char* name, const sfGlslMat4* matrixArray, size_t length)
	void sfShader_setFloatParameter(sfShader* shader, const char* name, float x)
	void sfShader_setFloat2Parameter(sfShader* shader, const char* name, float x, float y)
	void sfShader_setFloat3Parameter(sfShader* shader, const char* name, float x, float y, float z)
	void sfShader_setFloat4Parameter(sfShader* shader, const char* name, float x, float y, float z, float w)
	void sfShader_setVector2Parameter(sfShader* shader, const char* name, sfVector2f vector)
	void sfShader_setVector3Parameter(sfShader* shader, const char* name, sfVector3f vector)
	void sfShader_setColorParameter(sfShader* shader, const char* name, sfColor color)
	void sfShader_setTransformParameter(sfShader* shader, const char* name, sfTransform transform)
	void sfShader_setTextureParameter(sfShader* shader, const char* name, const sfTexture* texture)
	void sfShader_setCurrentTextureParameter(sfShader* shader, const char* name)
	unsigned int sfShader_getNativeHandle(const sfShader* shader)
	void sfShader_bind(const sfShader* shader)
	bint sfShader_isAvailable()
	bint sfShader_isGeometryAvailable()
