#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Types cimport *
from .PrimitiveType cimport *
from .RenderStates cimport *
from .Vertex cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/RenderTexture.h>":

	sfRenderTexture* sfRenderTexture_create(unsigned int width, unsigned int height, bint depthBuffer)
	void sfRenderTexture_destroy(sfRenderTexture* renderTexture)
	sfVector2u sfRenderTexture_getSize(const sfRenderTexture* renderTexture)
	bint sfRenderTexture_setActive(sfRenderTexture* renderTexture, bint active)
	void sfRenderTexture_display(sfRenderTexture* renderTexture)
	void sfRenderTexture_clear(sfRenderTexture* renderTexture, sfColor color)
	void sfRenderTexture_setView(sfRenderTexture* renderTexture, const sfView* view)
	const sfView* sfRenderTexture_getView(const sfRenderTexture* renderTexture)
	const sfView* sfRenderTexture_getDefaultView(const sfRenderTexture* renderTexture)
	sfIntRect sfRenderTexture_getViewport(const sfRenderTexture* renderTexture, const sfView* view)
	sfVector2f sfRenderTexture_mapPixelToCoords(const sfRenderTexture* renderTexture, sfVector2i point, const sfView* view)
	sfVector2i sfRenderTexture_mapCoordsToPixel(const sfRenderTexture* renderTexture, sfVector2f point, const sfView* view)
	void sfRenderTexture_drawSprite(sfRenderTexture* renderTexture, const sfSprite* object, const sfRenderStates* states)
	void sfRenderTexture_drawText(sfRenderTexture* renderTexture, const sfText* object, const sfRenderStates* states)
	void sfRenderTexture_drawShape(sfRenderTexture* renderTexture, const sfShape* object, const sfRenderStates* states)
	void sfRenderTexture_drawCircleShape(sfRenderTexture* renderTexture, const sfCircleShape* object, const sfRenderStates* states)
	void sfRenderTexture_drawConvexShape(sfRenderTexture* renderTexture, const sfConvexShape* object, const sfRenderStates* states)
	void sfRenderTexture_drawRectangleShape(sfRenderTexture* renderTexture, const sfRectangleShape* object, const sfRenderStates* states)
	void sfRenderTexture_drawVertexArray(sfRenderTexture* renderTexture, const sfVertexArray* object, const sfRenderStates* states)
	void sfRenderTexture_drawPrimitives(sfRenderTexture* renderTexture, const sfVertex* vertices, size_t vertexCount, sfPrimitiveType type, const sfRenderStates* states)
	void sfRenderTexture_pushGLStates(sfRenderTexture* renderTexture)
	void sfRenderTexture_popGLStates(sfRenderTexture* renderTexture)
	void sfRenderTexture_resetGLStates(sfRenderTexture* renderTexture)
	const sfTexture* sfRenderTexture_getTexture(const sfRenderTexture* renderTexture)
	void sfRenderTexture_setSmooth(sfRenderTexture* renderTexture, bint smooth)
	bint sfRenderTexture_isSmooth(const sfRenderTexture* renderTexture)
	void sfRenderTexture_setRepeated(sfRenderTexture* renderTexture, bint repeated)
	bint sfRenderTexture_isRepeated(const sfRenderTexture* renderTexture)
	bint sfRenderTexture_generateMipmap(sfRenderTexture* renderTexture)
