#from Export cimport *
from .Types cimport *
from .Transform cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Graphics/Transformable.h>":

	sfTransformable* sfTransformable_create()
	sfTransformable* sfTransformable_copy(const sfTransformable* transformable)
	void sfTransformable_destroy(sfTransformable* transformable)
	void sfTransformable_setPosition(sfTransformable* transformable, sfVector2f position)
	void sfTransformable_setRotation(sfTransformable* transformable, float angle)
	void sfTransformable_setScale(sfTransformable* transformable, sfVector2f scale)
	void sfTransformable_setOrigin(sfTransformable* transformable, sfVector2f origin)
	sfVector2f sfTransformable_getPosition(const sfTransformable* transformable)
	float sfTransformable_getRotation(const sfTransformable* transformable)
	sfVector2f sfTransformable_getScale(const sfTransformable* transformable)
	sfVector2f sfTransformable_getOrigin(const sfTransformable* transformable)
	void sfTransformable_move(sfTransformable* transformable, sfVector2f offset)
	void sfTransformable_rotate(sfTransformable* transformable, float angle)
	void sfTransformable_scale(sfTransformable* transformable, sfVector2f factors)
	sfTransform sfTransformable_getTransform(const sfTransformable* transformable)
	sfTransform sfTransformable_getInverseTransform(const sfTransformable* transformable)
