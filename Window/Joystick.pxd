#from Export cimport *
from .JoystickIdentification cimport *


cdef extern from "<SFML/Window/Joystick.h>":

	cdef enum:
		sfJoystickCount       = 8,
		sfJoystickButtonCount = 32,
		sfJoystickAxisCount   = 8

	ctypedef enum sfJoystickAxis:
		sfJoystickX,
		sfJoystickY,
		sfJoystickZ,
		sfJoystickR,
		sfJoystickU,
		sfJoystickV,
		sfJoystickPovX,
		sfJoystickPovY

	bint sfJoystick_isConnected(unsigned int joystick)
	unsigned int sfJoystick_getButtonCount(unsigned int joystick)
	bint sfJoystick_hasAxis(unsigned int joystick, sfJoystickAxis axis)
	bint sfJoystick_isButtonPressed(unsigned int joystick, unsigned int button)
	float sfJoystick_getAxisPosition(unsigned int joystick, sfJoystickAxis axis)
	sfJoystickIdentification sfJoystick_getIdentification(unsigned int joystick)
	void sfJoystick_update()
