#from Export cimport *
from .FontInfo cimport *
from .Glyph cimport *
from .Types cimport *
from System.InputStream cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/Font.h>":

	sfFont* sfFont_createFromFile(const char* filename)
	sfFont* sfFont_createFromMemory(const void* data, size_t sizeInBytes)
	sfFont* sfFont_createFromStream(sfInputStream* stream)
	sfFont* sfFont_copy(const sfFont* font)
	void sfFont_destroy(sfFont* font)
	sfGlyph sfFont_getGlyph(sfFont* font, unsigned int codePoint, unsigned int characterSize, bint bold, float outlineThickness)
	float sfFont_getKerning(sfFont* font, unsigned int first, unsigned int second, unsigned int characterSize)
	float sfFont_getLineSpacing(sfFont* font, unsigned int characterSize)
	float sfFont_getUnderlinePosition(sfFont* font, unsigned int characterSize)
	float sfFont_getUnderlineThickness(sfFont* font, unsigned int characterSize)
	const sfTexture* sfFont_getTexture(sfFont* font, unsigned int characterSize)
	sfFontInfo sfFont_getInfo(const sfFont* font)
