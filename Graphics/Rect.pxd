#form Export cimport *


cdef extern from "<SFML/Graphics/Rect.h>":

	ctypedef struct sfFloatRect:
		float left
		float top
		float width
		float height


	ctypedef struct sfIntRect:
		int left
		int top
		int width
		int height

	bint sfFloatRect_contains(const sfFloatRect* rect, float x, float y)
	bint sfIntRect_contains(const sfIntRect* rect, int x, int y)
	bint sfFloatRect_intersects(const sfFloatRect* rect1, const sfFloatRect* rect2, sfFloatRect* intersection)
	bint sfIntRect_intersects(const sfIntRect* rect1, const sfIntRect* rect2, sfIntRect* intersection)
