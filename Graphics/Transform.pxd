	#from Export cimport *
from .Rect cimport *
from .Types cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Graphics/Transform.h>":

	ctypedef struct sfTransform:
		float matrix[9]

	const sfTransform sfTransform_Identity
	sfTransform sfTransform_fromMatrix(float a00, float a01, float a02, float a10, float a11, float a12, float a20, float a21, float a22)
	void sfTransform_getMatrix(const sfTransform* transform, float* matrix)
	sfTransform sfTransform_getInverse(const sfTransform* transform)
	sfVector2f sfTransform_transformPoint(const sfTransform* transform, sfVector2f point)
	sfFloatRect sfTransform_transformRect(const sfTransform* transform, sfFloatRect rectangle)
	void sfTransform_combine(sfTransform* transform, const sfTransform* other)
	void sfTransform_translate(sfTransform* transform, float x, float y)
	void sfTransform_rotate(sfTransform* transform, float angle)
	void sfTransform_rotateWithCenter(sfTransform* transform, float angle, float centerX, float centerY)
	void sfTransform_scale(sfTransform* transform, float scaleX, float scaleY)
	void sfTransform_scaleWithCenter(sfTransform* transform, float scaleX, float scaleY, float centerX, float centerY)
