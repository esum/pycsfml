#from Export cimport *
from .Color cimport *
from System.Vector2 cimport *


cdef extern from "<SFML/Graphics/Vertex.h>":

	ctypedef struct sfVertex:
		sfVector2f position
		sfColor    color
		sfVector2f texCoords
