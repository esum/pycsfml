#from .Export cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Window/VideoMode.h>":

	ctypedef struct sfVideoMode:
		unsigned int width
		unsigned int height
		unsigned int bitsPerPixel

	sfVideoMode sfVideoMode_getDesktopMode()
	sfVideoMode* sfVideoMode_getFullscreenModes(size_t* count)
	bint sfVideoMode_isValid(sfVideoMode mode)
