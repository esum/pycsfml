#from .Export cimport *


cdef extern from "<SFML/System/Time.h>":

	ctypedef struct sfTime:
		long long microseconds

	sfTime sfTime_Zero
	float sfTime_asSeconds(sfTime time)
	int sfTime_asMilliseconds(sfTime time)
	long long sfTime_asMicroseconds(sfTime time)
	sfTime sfSeconds(float amount)
	sfTime sfMilliseconds(int amount)
	sfTime sfMicroseconds(long long amount)
