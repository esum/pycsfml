#from Export cimport *
from .Color cimport *
from .Rect cimport *
from .Types cimport *
from .PrimitiveType cimport *
from .RenderStates cimport *
from .Vertex cimport *
from Window.Event cimport *
from Window.VideoMode cimport *
from Window.WindowHandle cimport *
from Window.Window cimport *
from System.Vector2 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Graphics/RenderWindow.h>":

	sfRenderWindow* sfRenderWindow_create(sfVideoMode mode, const char* title, unsigned int style, const sfContextSettings* settings)
	sfRenderWindow* sfRenderWindow_createUnicode(sfVideoMode mode, const unsigned int* title, unsigned int style, const sfContextSettings* settings)
	sfRenderWindow* sfRenderWindow_createFromHandle(sfWindowHandle handle, const sfContextSettings* settings)
	void sfRenderWindow_destroy(sfRenderWindow* renderWindow)
	void sfRenderWindow_close(sfRenderWindow* renderWindow)
	bint sfRenderWindow_isOpen(const sfRenderWindow* renderWindow)
	sfContextSettings sfRenderWindow_getSettings(const sfRenderWindow* renderWindow)
	bint sfRenderWindow_pollEvent(sfRenderWindow* renderWindow, sfEvent* event)
	bint sfRenderWindow_waitEvent(sfRenderWindow* renderWindow, sfEvent* event)
	sfVector2i sfRenderWindow_getPosition(const sfRenderWindow* renderWindow)
	void sfRenderWindow_setPosition(sfRenderWindow* renderWindow, sfVector2i position)
	sfVector2u sfRenderWindow_getSize(const sfRenderWindow* renderWindow)
	void sfRenderWindow_setSize(sfRenderWindow* renderWindow, sfVector2u size)
	void sfRenderWindow_setTitle(sfRenderWindow* renderWindow, const char* title)
	void sfRenderWindow_setUnicodeTitle(sfRenderWindow* renderWindow, const unsigned int* title)
	void sfRenderWindow_setIcon(sfRenderWindow* renderWindow, unsigned int width, unsigned int height, const unsigned char* pixels)
	void sfRenderWindow_setVisible(sfRenderWindow* renderWindow, bint visible)
	void sfRenderWindow_setVerticalSyncEnabled(sfRenderWindow* renderWindow, bint enabled)
	void sfRenderWindow_setMouseCursorVisible(sfRenderWindow* renderWindow, bint show)
	void sfRenderWindow_setMouseCursorGrabbed(sfRenderWindow* renderWindow, bint grabbed)
	void sfRenderWindow_setKeyRepeatEnabled(sfRenderWindow* renderWindow, bint enabled)
	void sfRenderWindow_setFramerateLimit(sfRenderWindow* renderWindow, unsigned int limit)
	void sfRenderWindow_setJoystickThreshold(sfRenderWindow* renderWindow, float threshold)
	bint sfRenderWindow_setActive(sfRenderWindow* renderWindow, bint active)
	void sfRenderWindow_requestFocus(sfRenderWindow* renderWindow)
	bint sfRenderWindow_hasFocus(const sfRenderWindow* renderWindow)
	void sfRenderWindow_display(sfRenderWindow* renderWindow)
	sfWindowHandle sfRenderWindow_getSystemHandle(const sfRenderWindow* renderWindow)
	void sfRenderWindow_clear(sfRenderWindow* renderWindow, sfColor color)
	void sfRenderWindow_setView(sfRenderWindow* renderWindow, const sfView* view)
	const sfView* sfRenderWindow_getView(const sfRenderWindow* renderWindow)
	const sfView* sfRenderWindow_getDefaultView(const sfRenderWindow* renderWindow)
	sfIntRect sfRenderWindow_getViewport(const sfRenderWindow* renderWindow, const sfView* view)
	sfVector2f sfRenderWindow_mapPixelToCoords(const sfRenderWindow* renderWindow, sfVector2i point, const sfView* view)
	sfVector2i sfRenderWindow_mapCoordsToPixel(const sfRenderWindow* renderWindow, sfVector2f point, const sfView* view)
	void sfRenderWindow_drawSprite(sfRenderWindow* renderWindow, const sfSprite* object, const sfRenderStates* states)
	void sfRenderWindow_drawText(sfRenderWindow* renderWindow, const sfText* object, const sfRenderStates* states)
	void sfRenderWindow_drawShape(sfRenderWindow* renderWindow, const sfShape* object, const sfRenderStates* states)
	void sfRenderWindow_drawCircleShape(sfRenderWindow* renderWindow, const sfCircleShape* object, const sfRenderStates* states)
	void sfRenderWindow_drawConvexShape(sfRenderWindow* renderWindow, const sfConvexShape* object, const sfRenderStates* states)
	void sfRenderWindow_drawRectangleShape(sfRenderWindow* renderWindow, const sfRectangleShape* object, const sfRenderStates* states)
	void sfRenderWindow_drawVertexArray(sfRenderWindow* renderWindow, const sfVertexArray* object, const sfRenderStates* states)
	void sfRenderWindow_drawPrimitives(sfRenderWindow* renderWindow, const sfVertex* vertices, size_t vertexCount, sfPrimitiveType type, const sfRenderStates* states)
	void sfRenderWindow_pushGLStates(sfRenderWindow* renderWindow)
	void sfRenderWindow_popGLStates(sfRenderWindow* renderWindow)
	void sfRenderWindow_resetGLStates(sfRenderWindow* renderWindow)
	sfImage* sfRenderWindow_capture(const sfRenderWindow* renderWindow)
	sfVector2i sfMouse_getPositionRenderWindow(const sfRenderWindow* relativeTo)
	void sfMouse_setPositionRenderWindow(sfVector2i position, const sfRenderWindow* relativeTo)
	sfVector2i sfTouch_getPositionRenderWindow(unsigned int finger, const sfRenderWindow* relativeTo)
