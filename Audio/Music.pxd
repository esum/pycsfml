#from .Export cimport *
from .SoundStatus cimport *
from .Types cimport *
from System.InputStream cimport *
from System.Time cimport *
from System.Vector3 cimport *
from libc.stddef cimport size_t


cdef extern from "<SFML/Audio/Music.h>":

	sfMusic* sfMusic_createFromFile(const char* filename)
	sfMusic* sfMusic_createFromMemory(const void* data, size_t sizeInBytes)
	sfMusic* sfMusic_createFromStream(sfInputStream* stream)
	void sfMusic_destroy(sfMusic* music)
	void sfMusic_setLoop(sfMusic* music, bint loop)
	bint sfMusic_getLoop(const sfMusic* music)
	sfTime sfMusic_getDuration(const sfMusic* music)
	void sfMusic_play(sfMusic* music)
	void sfMusic_pause(sfMusic* music)
	void sfMusic_stop(sfMusic* music)
	unsigned int sfMusic_getChannelCount(const sfMusic* music)
	unsigned int sfMusic_getSampleRate(const sfMusic* music)
	sfSoundStatus sfMusic_getStatus(const sfMusic* music)
	sfTime sfMusic_getPlayingOffset(const sfMusic* music)
	void sfMusic_setPitch(sfMusic* music, float pitch)
	void sfMusic_setVolume(sfMusic* music, float volume)
	void sfMusic_setPosition(sfMusic* music, sfVector3f position)
	void sfMusic_setRelativeToListener(sfMusic* music, bint relative)
	void sfMusic_setMinDistance(sfMusic* music, float distance)
	void sfMusic_setAttenuation(sfMusic* music, float attenuation)
	void sfMusic_setPlayingOffset(sfMusic* music, sfTime timeOffset)
	float sfMusic_getPitch(const sfMusic* music)
	float sfMusic_getVolume(const sfMusic* music)
	sfVector3f sfMusic_getPosition(const sfMusic* music)
	bint sfMusic_isRelativeToListener(const sfMusic* music)
	float sfMusic_getMinDistance(const sfMusic* music)
	float sfMusic_getAttenuation(const sfMusic* music)
