#from .Export cimport *


cdef extern from "<SFML/System/Vector3.h>":

	ctypedef struct sfVector3f:
		float x
		float y
		float z
