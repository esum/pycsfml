.PHONY: all
all:
	@echo "Building SFML binding..."
	C_INCLUDE_PATH=include python3 setup.py build_ext -i
	cp sf.cpython-35m-darwin.so ..

.PHONY: clean
clean:
	rm -f sf.c
	rm -f sf.cpython*.so
